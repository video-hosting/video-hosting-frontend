export const startYaMetric = () => {
    setTimeout(() => {
        // @ts-ignore
        window["ym"] = window["ym"] || function () {
            // @ts-ignore
            (window["ym"].a = window["ym"].a || []).push(arguments)
        };
        // @ts-ignore
        window["ym"].l = 1 * new Date();
        for (var j = 0; j < document.scripts.length; j++) {
            if (document.scripts[j].src === "https://mc.yandex.ru/metrika/tag.js") {
                return;
            }
        }
        let k = document.createElement("script")
        let a = document.getElementsByTagName("script")[0]
        // @ts-ignore
        k.async = 1
        k.src = "https://mc.yandex.ru/metrika/tag.js"
        // @ts-ignore
        a.parentNode.insertBefore(k, a)

        // @ts-ignore
        window.ym(95834018, "init", {
            clickmap: true,
            trackLinks: true,
            accurateTrackBounce: true
        });
    }, 0)
}

let isActiveAnalytics = false

export const addAnalytics = () => {
    if (!isActiveAnalytics) {
        startYaMetric()

        isActiveAnalytics = true

        setTimeout(() => {
            document.removeEventListener('mousemove', addAnalytics)
            document.removeEventListener('touchstart', addAnalytics)
        }, 100)
    }
}

