export class AuthController {
    constructor() {}

    setAccessToken(token: string) {
        localStorage.setItem('accessToken', token)
    }
    getAccessToken() {
        return localStorage.getItem('accessToken')
    }

    removeAccessToken() {
        localStorage.removeItem('accessToken')
    }
}

const authController = new AuthController()

export {authController}