import {ThemeE} from "@/types";

export class ThemeController {
    theme: ThemeE

    constructor() {
        this.theme = localStorage.getItem('theme') as ThemeE || ThemeE.light
    }

    setTheme(theme: ThemeE) {
        localStorage.setItem('theme', theme)
        document.cookie = `theme=${theme}`
    }
}