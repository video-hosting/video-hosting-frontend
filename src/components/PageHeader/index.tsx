'use client'
import React, {useContext, useLayoutEffect, useState} from "react";
import Link from "next/link";
import {Button} from "primereact/button";
import {PrimeReactContext} from "primereact/api";
import {ThemeE} from "@/types";
import {APP_THEME_ID} from "@/constants";
import {ThemeController} from "@/utils/ThemeController";
import {AuthContext} from "@/components/Provider";
import {ThemeToggle} from "@/components/PageHeader/components/ThemeToggle";
import {AuthNavBlock} from "@/components/PageHeader/components/AuthNavBlock";
import {api} from "@/services/api";
import {MenuButton} from "@/components/PageHeader/components/MenuButton";
import {MainSearch} from "@/components/PageHeader/components/MainSearch";
import {useWindowWidth} from "@/hooks";
import styles from './styles.module.css'

type PageHeaderP = {
    defaultTheme: ThemeE
}

export const PageHeader:React.FC<PageHeaderP> = ({defaultTheme}) => {
    const {changeTheme} = useContext(PrimeReactContext);
    const {user, logout, showError, isLoadingAuth} = useContext(AuthContext);
    const [theme, setTheme] = useState<ThemeE>(defaultTheme);
    const {width} = useWindowWidth()

    const changeMyTheme = () => {
        theme === ThemeE.light
            ? changeTheme?.(ThemeE.light, ThemeE.dark, APP_THEME_ID)
            : changeTheme?.(ThemeE.dark, ThemeE.light, APP_THEME_ID)
        setTheme(theme === ThemeE.light ? ThemeE.dark : ThemeE.light)
        const themeController = new ThemeController()
        themeController.setTheme(theme === ThemeE.light ? ThemeE.dark : ThemeE.light)
    };

    const logoutHandler = async () => {
        try {
            await api.logout()
            logout()
        } catch (err) {
            showError('Не удалось выйти')
        }
    }

    useLayoutEffect(() => {
        const themeController = new ThemeController()
        themeController.theme === ThemeE.light
            ? changeTheme?.(ThemeE.dark, ThemeE.light, APP_THEME_ID)
            : changeTheme?.(ThemeE.light, ThemeE.dark, APP_THEME_ID)
        setTheme(themeController.theme)
    }, [])

    return <nav className={styles.navigationHeader}>
        <div className="flex align-items-center gap-2">
            <MenuButton theme={theme} changeMyTheme={changeMyTheme} logoutHandler={logoutHandler}/>
            <MainSearch/>
        </div>
        <div className={styles.rightNavBlock}>
            {user && <Link href="/add-video">
                <Button aria-label="add video" text icon="pi pi-plus"/>
            </Link>}
            <ThemeToggle changeMyTheme={changeMyTheme} theme={theme}/>
            {!isLoadingAuth && <AuthNavBlock user={user} logoutHandler={logoutHandler}/>}
        </div>
    </nav>
}