import React from "react";
import {Button} from "primereact/button";
import {ThemeE} from "@/types";
import styles from './styles.module.css'

type ThemeToggleP = {
    theme: ThemeE;
    changeMyTheme: () => void
}

export const ThemeToggle:React.FC<ThemeToggleP> = ({changeMyTheme, theme}) => {
    return <div>
        {theme === ThemeE.light && <Button aria-label="Change theme" onClick={changeMyTheme} text icon="pi pi-moon"/>}
        {theme === ThemeE.dark && <Button aria-label="Change theme" onClick={changeMyTheme} text icon="pi pi-sun"/>}
    </div>
}