import React, {useContext, useMemo, useState} from "react";
import {useRouter} from "next/navigation";
import {Button} from "primereact/button";
import {classNames} from "primereact/utils";
import {Sidebar} from "primereact/sidebar";
import {Menu} from "primereact/menu";
import {AuthContext} from "@/components/Provider";
import {getMenuItems} from "@/components/PageHeader/components/MenuButton/utils";
import styles from "./styles.module.css";
import {useWindowWidth} from "@/hooks";
import {ThemeE} from "@/types";

type MenuButtonP = {
    changeMyTheme: () => void;
    logoutHandler: () => void;
    theme: ThemeE;
}

export const MenuButton: React.FC<MenuButtonP> = ({changeMyTheme, logoutHandler, theme}) => {
    const [isOpenMenu, setIsOpenMenu] = useState<boolean>(false)
    const {user} = useContext(AuthContext)
    const router = useRouter()
    const {width} = useWindowWidth()

    const menuItem = useMemo(
        () => getMenuItems(router, user, width, theme, changeMyTheme, logoutHandler, () => setIsOpenMenu(false)),
        [router, user, width, changeMyTheme]
    )

    return <div>
        <Button
            aria-label="Menu"
            onClick={() => setIsOpenMenu(!isOpenMenu)}
            text
            icon="pi pi-bars"
            className={classNames("p-overlay-badge")}
            style={{fontSize: 14}}
        />
        <Sidebar
            visible={isOpenMenu}
            className={styles.sidebar}
            position="left"
            showCloseIcon={false}
            icons={<Button
                onClick={() => setIsOpenMenu(!isOpenMenu)}
                text
                icon="pi pi-bars"
                className="p-overlay-badge"
            />}
            onHide={() => setIsOpenMenu(false)}
        >
            <Menu className={styles.sideMenu} model={menuItem} />
        </Sidebar>
    </div>
}