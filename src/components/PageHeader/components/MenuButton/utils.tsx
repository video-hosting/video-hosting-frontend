import React from "react";
import {AppRouterInstance} from "next/dist/shared/lib/app-router-context";
import {IUserFromLogin} from "@/types/UserTypes";
import {MenuItem} from "primereact/menuitem";
import styles from './styles.module.css'
import {BASE_URL} from "@/services/api";
import {Image} from "primereact/image";
import {ThemeE} from "@/types";

type SubscribeChannelMenuItemP = {
    id: number;
    label: string;
    onClick: () => void;
}
const SubscribeChannelMenuItem: React.FC<SubscribeChannelMenuItemP> = ({id, label, onClick}) => {
    return <div className={styles.subscribeMenuItem} onClick={onClick}>
        <Image
            imageClassName={styles.avatar}
            src={`${BASE_URL}/api/avatar/${id}`}
            alt="avatar"
            width="32"
            height="32"
        />
        {label}
    </div>
}

export const getMenuItems = (
    router: AppRouterInstance,
    user: IUserFromLogin | null,
    width: number,
    theme: ThemeE,
    changeMyTheme: () => void,
    logoutHandler: () => void,
    cb: () => void
) => {
    const res: MenuItem[] = [
        {
            label: 'Главная',
            icon: 'pi pi-home',
            command: () => {
                router.push('/')
                cb()
            }
        },
    ]
    if (user) {
        res.push({
            label: 'Мои видео',
            icon: 'pi pi-list',
            command: () => {
                router.push(`/channel/${user.id}`)
                cb()
            }
        })
        if (user.role.includes('admin')) {
            res.push({
                label: 'Модерация видео',
                icon: 'pi pi-list',
                command: () => {
                    router.push(`/moderate-video`)
                    cb()
                }
            })
        }
        res.push({
            label: 'Настройки',
            icon: 'pi pi-cog',
            command: () => {
                router.push('/settings')
                cb()
            }
        })
    }
    if (width <= 800) {
        res.push({
            label: 'Изменить тему',
            icon: theme === ThemeE.dark ? 'pi pi-sun' :  'pi pi-moon',
            command: () => changeMyTheme()
        })
        if (user) {
            res.push({
                label: 'Добавить видео',
                icon: 'pi pi-plus',
                command: () => {
                    router.push('/add-video')
                    cb()
                }
            })
            res.push({
                label: `${user.name} ${user.surname}`,
                icon: 'pi pi-user',
                items: [{
                        label: 'Выйти',
                        icon: 'pi pi-user',
                        command: () => {
                            logoutHandler()
                            cb()
                        }
                }]
            })
        } else {
            res.push({
                label: 'Войти',
                icon: 'pi pi-user',
                command: () => {
                    router.push('/login')
                    cb()
                }
            })
        }
    }
    if (user) {
        res.push({separator: true})
        res.push({
            label: 'Подписки',
            icon: 'pi pi-list',
            items: user.subscribes.map(s => ({
                template: <SubscribeChannelMenuItem
                    id={s.id}
                    label={s.nickname}
                    onClick={() => {
                        router.push(`/channel/${s.id}`)
                        cb()
                    }}
                />
            }))
        })
    }
    return res
}
