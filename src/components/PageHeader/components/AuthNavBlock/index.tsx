import Link from "next/link";
import {Button} from "primereact/button";
import React from "react";
import {IUserFromLogin} from "@/types/UserTypes";
import styles from "./styles.module.css";

type AuthNavBlockP = {
    user: IUserFromLogin | null;
    logoutHandler: () => void;
}

export const AuthNavBlock: React.FC<AuthNavBlockP> = ({user, logoutHandler}) => {
    return <div className={styles.authNavBlock}>
        {!user
            ? <Link href="/login">
                <Button className={styles.authBtn} aria-label="Войти" label="Войти" icon="pi pi-user"/>
            </Link>
            : <div className={styles.userNameBlock}>
                <span>{user.name} {user.surname}</span>
                <Button className={styles.authBtn} onClick={logoutHandler} aria-label="Log out" label="Выйти" icon="pi pi-user"/>
            </div>
        }
    </div>
}