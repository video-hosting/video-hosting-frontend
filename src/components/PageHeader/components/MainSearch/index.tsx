import React, {useState} from "react";
import {useRouter} from "next/navigation";
import {InputText} from "primereact/inputtext";
import {Button} from "primereact/button";
import styles from './styles.module.css'

export const MainSearch = () => {
    const [value, setValue] = useState<string>('');
    const router = useRouter();

    const submit = (e?: any) => {
        if (!e || e?.code === 'Enter') {
            router.push(`/search-results?q=${value}`)
        }
    }
    const onClick = () => {
        router.push(`/search-results?q=${value}`)
    }

    return <div className={styles.mainSearchContainer}>
        <span className="p-input-icon-left">
            <i className="pi pi-search"/>
            <InputText
                onKeyUp={(e) => submit(e)}
                onChange={(e) => setValue(e.target.value)}
                value={value}
                className={styles.searchInput}
                placeholder="Найти"
            />
        </span>
        <Button aria-label="Search" onClick={onClick} icon="pi pi-search" className={styles.searchBtn}/>
    </div>
}