export type OptionT = {
    name: string,
    rate: number
}
export const rates: OptionT[] = [
    {name: '0.25x', rate: 0.25},
    {name: '0.5x', rate: 0.5},
    {name: '0.75x', rate: 0.75},
    {name: '1x', rate: 1},
    {name: '1.25x', rate: 1.25},
    {name: '1.5x', rate: 1.5},
    {name: '1.75x', rate: 1.75},
    {name: '2x', rate: 1.75},
];

export const qualityValues: OptionT[] = [
    {name: '360p', rate: 360},
    {name: '480p', rate: 480},
    {name: '720p', rate: 720},
    {name: '1080p', rate: 1080},
];