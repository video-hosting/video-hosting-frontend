import React, {MouseEventHandler, TouchEventHandler, useEffect, useRef, useState} from "react";
import {formatTime} from "@/utils/formatTimeForVideo";
import styles from './styles.module.css'
import {useWindowWidth} from "@/hooks";

type MainVideoRangeP = {
    duration: number;
    currentTime: number;
    className: string | undefined | false;
    setCurrentTime: (v: number) => void;
}

export const MainVideoRange: React.FC<MainVideoRangeP> = ({
                                                              duration,
                                                              currentTime,
                                                              setCurrentTime,
                                                              className,
                                                          }) => {
    const [isVisibleTimeTooltip, setIsVisibleTimeTooltip] = useState<boolean>(false)
    const [isMovingSlider, setIsMovingSlider] = useState<boolean>(false)
    const refContainer = useRef<HTMLDivElement>(null)
    const timeTooltipRef = useRef<HTMLDivElement>(null)
    const activeRangeRef = useRef<HTMLDivElement>(null)
    const passiveRangeRef = useRef<HTMLDivElement>(null)

    const {width} = useWindowWidth()
    const onMouseMoveRange: MouseEventHandler<HTMLDivElement> = (e) => {
        const containerRect = refContainer.current?.getBoundingClientRect() as DOMRect
        const {width, left} = containerRect
        const sliderValue = (e.pageX - left) < 0 ? 0 : e.pageX - left
        if (passiveRangeRef.current) {
            passiveRangeRef.current.style.width = `${sliderValue > width ? width : sliderValue }px`
        }
        const rateValue = sliderValue > width ? width / width : sliderValue / width
        if (timeTooltipRef.current) {
            timeTooltipRef.current.style.left = `${sliderValue > width ? width : sliderValue}px`
            timeTooltipRef.current.innerText = formatTime(Math.round(rateValue * duration))
        }

    }

    const onMouseEnter: MouseEventHandler<HTMLDivElement> = (e) => {
        const containerRect = refContainer.current?.getBoundingClientRect()
        const timeTooltipValue = (e.pageX - (containerRect?.left || 0)) < 0 ? 0 : e.pageX - (containerRect?.left || 0)
        if (passiveRangeRef.current) {
            passiveRangeRef.current.style.width = `${timeTooltipValue}px`
        }
        setIsVisibleTimeTooltip(true)
        setTimeout(() => {
            if (timeTooltipRef.current) {
                timeTooltipRef.current.style.left = `${timeTooltipValue}px`
                timeTooltipRef.current.innerText = formatTime(Math.round((timeTooltipValue / (containerRect?.width || 0)) * duration))
            }
        }, 5)
    }
    const onMouseLeave = () => setIsVisibleTimeTooltip(false)

    const onClickRange: MouseEventHandler<HTMLDivElement> = (e) => {
        const containerRect = refContainer.current?.getBoundingClientRect() as DOMRect
        const {width, left} = containerRect
        const sliderValue = (e.pageX - left) < 0 ? 0 : e.pageX - left
        const rateValue = sliderValue > width ? width / width : sliderValue / width
        setCurrentTime(duration * rateValue)
    }

    const onTouchRange: TouchEventHandler<HTMLDivElement> = (e) => {
        const containerRect = refContainer.current?.getBoundingClientRect() as DOMRect
        const {width, left} = containerRect
        const sliderValue = (e.touches[0].pageX - left) < 0 ? 0 : e.touches[0].pageX - left
        const rateValue = sliderValue > width ? width / width : sliderValue / width
        setCurrentTime(duration * rateValue)
    }

    const onMouseDownSlider: MouseEventHandler<HTMLDivElement> = () => {
        setIsMovingSlider(true)
        setIsVisibleTimeTooltip(true)
        const moveCb = (e: MouseEvent) => {
            // @ts-ignore
            onMouseMoveRange(e)
            // @ts-ignore
            onClickRange(e)
        }
        const upCb = () => {
            setIsMovingSlider(false)
            setIsVisibleTimeTooltip(false)

            window.removeEventListener('mouseup', upCb)
            window.removeEventListener('mousemove', moveCb)
        }
        window.addEventListener('mousemove', moveCb)
        window.addEventListener('mouseup', upCb)
    }

    const onTouch: TouchEventHandler<HTMLDivElement> = (e) => {
        setIsMovingSlider(true)
        setIsVisibleTimeTooltip(true)
        const moveCb = (e: MouseEvent) => {
            // @ts-ignore
            onMouseMoveRange(e)
            // @ts-ignore
            onTouchRange(e)
        }
        const upCb = () => {
            setIsMovingSlider(false)
            setIsVisibleTimeTooltip(false)

            window.removeEventListener('touchend', upCb)
            // @ts-ignore
            window.removeEventListener('touchmove', moveCb)
        }
        // @ts-ignore
        window.addEventListener('touchmove', moveCb)
        window.addEventListener('touchend', upCb)
    }

    useEffect(() => {
        if (activeRangeRef.current && refContainer.current) {
            const {width} = refContainer.current.getBoundingClientRect()
            const activeWidth = (currentTime / duration) * width

            activeRangeRef.current.style.width = `${activeWidth || 0}px`
        }
    }, [currentTime, duration, width])

    return <div
        ref={refContainer}
        onMouseMove={onMouseMoveRange}
        onMouseEnter={onMouseEnter}
        onMouseLeave={onMouseLeave}
        onClick={onClickRange}
        className={`${styles.mainVideoRangeContainer} ${className}`}
        draggable={false}
    >
        <div className={styles.mainVideoRangeWrapper}>
            <div draggable={false} ref={passiveRangeRef} className={styles.passiveRange}/>
            <div draggable={false} ref={activeRangeRef} className={styles.activeRange}>
                <div
                    draggable={false}
                    onMouseDown={onMouseDownSlider}
                    onTouchStart={onTouch}
                    className={styles.rangeSlider}
                />
            </div>
            {(isVisibleTimeTooltip || isMovingSlider) && <div
                draggable={false}
                ref={timeTooltipRef}
                className={styles.timeTooltip}
            />}
        </div>
    </div>
}