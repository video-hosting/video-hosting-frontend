import React, {useEffect, useRef, useState} from "react";
import * as localforage from "localforage";
import {Slider, SliderChangeEvent} from "primereact/slider";
import {Button} from "primereact/button";
import {classNames} from "primereact/utils";
import {Dropdown, DropdownChangeEvent} from "primereact/dropdown";
import {formatTime} from "@/utils/formatTimeForVideo";
import {BASE_URL} from "@/services/api";
import {MainVideoRange} from "@/components/VideoPlayer/components/MainVideoRange";
import {OptionT, qualityValues, rates} from "@/components/VideoPlayer/options";
import styles from "./styles.module.css";
import {useWindowWidth} from "@/hooks";

type VideoPlayerP = {
    videoId: string;
    duration: number;
}

export const VideoPlayer: React.FC<VideoPlayerP> = ({videoId, duration}) => {
    const [isPlaying, setIsPlaying] = useState(false);
    const [volume, setVolume] = useState(1);
    const [isFullScreen, setIsFullScreen] = useState(false);
    const [isLooping, setIsLooping] = useState(false);
    const [playbackRate, setPlaybackRate] = useState<OptionT>(rates[3]);
    const [quality, setQuality] = useState<OptionT>(qualityValues[3]);
    const [currentTime, setCurrentTime] = useState<number>(0)
    const [durationValue, setDurationValue] = useState<number>(duration)
    const {width} = useWindowWidth()

    const videoContainerRef = useRef<HTMLDivElement | null>(null);
    const videoRef = useRef<HTMLVideoElement | null>(null);

    const height = isFullScreen
        ? '100%'
        : videoContainerRef.current ? `${videoContainerRef.current?.getBoundingClientRect().width * 9 / 16}px` : undefined

    const handlePlaybackRateChange = (event: DropdownChangeEvent) => {
        if (videoRef.current) {
            setPlaybackRate(event.value);
            videoRef.current.playbackRate = event.value.rate;
        }
    };
    const onChangeQuality = (event: DropdownChangeEvent) => {
        videoRef.current?.pause();
        setIsPlaying(false);
        setQuality(event.value)
    }
    const handleLoop = () => {
        if (videoRef.current) {
            setIsLooping(!isLooping);
            videoRef.current.loop = !isLooping;
        }
    };
    const handleFullScreen = () => {
        if (!isFullScreen) {
            videoContainerRef.current?.requestFullscreen();
        } else {
            document.exitFullscreen();
        }
    };
    const handleVolumeChange = (event: SliderChangeEvent) => {
        if (videoRef.current) {
            setVolume(event.value as number);
            videoRef.current.volume = event.value as number;
        }
    };
    const handlePlayPause = () => {
        setIsPlaying((prev) => {
            if (prev) {
                videoRef.current?.pause();
            } else {
                videoRef.current?.play();
            }
            return !prev
        })
    };
    const onClickVolume = () => {
        if (volume === 0) {
            if (videoRef.current) {
                setVolume(1)
                videoRef.current.volume = 1;
            }
        } else {
            if (videoRef.current) {
                setVolume(0)
                videoRef.current.volume = 0;
            }
        }
    }

    const onChangeProgress = (value: number) => {
        setCurrentTime(value)
        if (videoRef.current) {
            videoRef.current.currentTime = value
        }
    }

    const onLoadedMetadata = () => {
        setDurationValue(videoRef.current?.duration || duration)
        if (videoRef.current) {
            videoRef.current.currentTime = currentTime;
            videoRef.current.volume = volume;
            videoRef.current.playbackRate = playbackRate.rate;
            videoRef.current.play()
                .then(() => setIsPlaying(true))
                .catch(() => setIsPlaying(false))
        }
    }

    useEffect(() => {
        if (isPlaying) {
            const interval = setInterval(() => {
                setCurrentTime(videoRef.current?.currentTime ? videoRef.current?.currentTime : 0)
                localforage.setItem(videoId, videoRef.current?.currentTime);
            }, playbackRate.rate * 200)
            return () => clearInterval(interval)
        }
    }, [playbackRate, isPlaying]);

    useEffect(() => {
        setTimeout(() => {
            setPlaybackRate(p => ({...p}))
        }, 100)
        const changeFullscreen = () => {
            setIsFullScreen(prev => !prev)
        }
        const onKeyUp = (e: KeyboardEvent) => {
            if (document.activeElement?.tagName !== 'INPUT' && document.activeElement?.tagName !== 'TEXTAREA') {
                if (e.code === 'Space') {
                    e.preventDefault();
                    handlePlayPause()
                }
                if (e.code === 'KeyF') {
                    e.preventDefault();
                    setIsFullScreen(prev => {
                        if (!prev) {
                            videoContainerRef.current?.requestFullscreen();
                        } else {
                            document.exitFullscreen();
                        }
                        return prev
                    })
                }
            }
        }
        document.addEventListener('keypress', onKeyUp)
        document.addEventListener('fullscreenchange', changeFullscreen, false);
        (async function (){
            const num = await localforage.getItem(videoId)
            if (num && videoRef.current) {
                setCurrentTime(+num)
                videoRef.current.currentTime = +num;
            }
        })()
        return () => {
            window.removeEventListener('keyup', onKeyUp)
            window.removeEventListener('fullscreenchange', changeFullscreen)
        }
    }, []);

    return <div className={styles.videoContainer} ref={videoContainerRef}>
        <video
            src={`${BASE_URL}/api/video/file/${videoId}?quality=${quality.rate}`}
            draggable={false}
            onLoadedMetadata={onLoadedMetadata}
            preload='auto'
            autoPlay
            onClick={handlePlayPause}
            onEnded={() => {
                setCurrentTime(durationValue);
                localforage.setItem(videoId, 0);
            }}
            ref={videoRef}
            height={height}
            width="100%"
            controls={false}
            onPlay={() => setIsPlaying(true)}
            onPause={() => setIsPlaying(false)}
        />
        <MainVideoRange
            className={isPlaying && styles.opacity}
            currentTime={currentTime}
            duration={durationValue}
            setCurrentTime={onChangeProgress}
        />
        <div draggable={false} className={classNames(styles.controls, isPlaying && styles.opacity)}>
            <div className={styles.left}>
                <Button
                    className={styles.controlBtn}
                    text
                    onClick={handlePlayPause}
                    icon={isPlaying ? "pi pi-pause" : "pi pi-play"}
                />
                <Button
                    className={styles.controlBtn}
                    text
                    onClick={onClickVolume}
                    icon={volume > 0 ? "pi pi-volume-up" : "pi pi-volume-off"}
                />
                <Slider
                    className={classNames("w-4rem", styles.range)}
                    min={0}
                    max={1}
                    step={0.01}
                    value={volume}
                    onChange={handleVolumeChange}
                />
                <Button
                    className={styles.controlBtn}
                    text
                    onClick={handleLoop}
                    icon={isLooping ? "pi pi-times" : "pi pi-replay"}
                />
                {width > 420 && <Dropdown
                    value={playbackRate}
                    onChange={handlePlaybackRateChange}
                    options={rates}
                    optionLabel="name"
                    className={styles.playbackRates}
                    panelClassName={styles.panelPlaybackRates}
                />}
                <div className={styles.times}>
                    <span>{formatTime(currentTime)}</span>
                    <span>/</span>
                    <span>{formatTime(duration)}</span>
                </div>

            </div>
            <div className={styles.right}>
                <Dropdown
                    value={quality}
                    onChange={onChangeQuality}
                    options={qualityValues}
                    optionLabel="name"
                    className={styles.playbackRates}
                    panelClassName={styles.panelPlaybackRates}
                />
                <Button
                    className={styles.controlBtn}
                    text
                    onClick={handleFullScreen}
                    icon={isFullScreen ? "pi pi-window-minimize" : "pi pi-window-maximize"}
                />
            </div>
        </div>
    </div>
}