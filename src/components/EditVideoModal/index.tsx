import React from "react";
import {Dialog} from "primereact/dialog";
import {IVideoItem} from "@/types/VideoTypes";
import {AddVideoData, InputsType} from "@/components/AddVideoContent/components/AddVideoData";

type EditVideoModalP = {
    visible: boolean;
    videoData: IVideoItem | null;
    onCancel: () => void;
    onEdit: (data: InputsType) => void;
}

export const EditVideoModal: React.FC<EditVideoModalP> = ({
                                                              visible,
                                                              onCancel,
                                                              onEdit,
                                                              videoData,
                                                          }) => {
    return <Dialog
        header="Изменение видео"
        visible={visible && !!videoData}
        style={{width: '80vw'}}
        onHide={onCancel}
    >
        {videoData && <AddVideoData
            saveVideoData={() => {}}
            values={{
                isAgeRelatedContent: !!videoData.isAgeRelatedContent,
                videoName: videoData.name,
                description: videoData.description,
                tags: videoData.tags,
                isPrivate: videoData.isPrivate || false
            }}
            submit={onEdit}
        />}
    </Dialog>
}