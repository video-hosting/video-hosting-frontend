'use client'
import React from "react";
import {Image} from "primereact/image";
import {classNames} from "primereact/utils";
import {useRouter} from "next/navigation";
import {BASE_URL} from "@/services/api";
import styles from "./styles.module.css";

type PreviewVideoP = {
    id: number;
    duration: number;
}

export const PreviewVideo: React.FC<PreviewVideoP> = ({id, duration}) => {
    const router = useRouter()

    const toVideo = () => router.push(`/video/${id}`)

    return <div onClick={toVideo} className={styles.previewVideoContainer}>
        <Image
            className={styles.previewImage}
            src={`${BASE_URL}/api/video/preview-image/${id}`}
            alt="avatar"
        />
        <span className={classNames("pi", "pi-play", styles.hoverVideo)}></span>
        <p className={styles.duration}>{new Date(duration * 1000).toISOString().slice(11, 19)}</p>
    </div>
}