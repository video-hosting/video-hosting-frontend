import React from "react";
import {ProfileSettings} from "@/components/Settings/ProfileSettings";

type SettingsP = {
    activeIndex: number;
}

export const Settings: React.FC<SettingsP> = ({activeIndex}) => {
    switch (activeIndex) {
        case 1:
            return <ProfileSettings/>
        default:
            return <ProfileSettings/>
    }
}