import React, {useContext, useState} from "react";
import {classNames} from "primereact/utils";
import { Image } from 'primereact/image';
import {AuthContext} from "@/components/Provider";
import {api, BASE_URL} from "@/services/api";
import styles from './styles.module.css'

export const ProfileSettings = () => {
    const [, setUploadedAvatar] = useState<File | null>(null)
    const {user, showError, showSuccess} = useContext(AuthContext);
    const [imageUrl, setImageUrl] = useState<string>(`${BASE_URL}/api/avatar/${user?.id}`)

    const uploadAvatar: React.InputHTMLAttributes<HTMLInputElement>['onChange'] = async (e) => {
        try {
            if (e.target.files?.length) {
                const data = new FormData()
                data.append('file', e.target.files[0])
                await api.updateAvatar(data)
                setUploadedAvatar(e.target.files[0])
                setImageUrl('')
                setTimeout(() => setImageUrl(`${BASE_URL}/api/avatar/${user?.id}`), 100)
                showSuccess('Аватар успешно изменен')
            }
        } catch (err:any) {
            showError('Не удалось загрузить аватар', err?.response?.data?.message)
        }
    }

    return <div className="flex flex-column gap-2">
        {user && <label>
            <div className={styles.avatarContainer}>
                <Image
                    src={imageUrl}
                    alt="avatar"
                    width="160"
                    height="160"
                />
                <span className={classNames("pi", "pi-upload", styles.editAvatar)}></span>
            </div>
            <input accept="image/png, image/gif, image/jpeg, image/webp" onChange={uploadAvatar} type="file"
                   style={{display: 'none'}}/>
        </label>}

        {!user?.accountIsConfirm && <span className="text-red-500">Аккаунт не подтвержден!</span>}
        <div>
            <span>Логин:</span><span>{user?.login}</span>
        </div>
        <div>
            <span>Email:</span><span>{user?.email}</span>
        </div>
        <div>
            <span>Имя:</span><span>{user?.name}</span>
        </div>
        <div>
            <span>Фамилия:</span><span>{user?.surname}</span>
        </div>
    </div>
}