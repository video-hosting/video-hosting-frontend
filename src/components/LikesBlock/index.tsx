import React, {useContext} from "react";
import {Button} from "primereact/button";
import styles from "./styles.module.css";
import {classNames} from "primereact/utils";
import {AuthContext} from "@/components/Provider";

type LikesBlockP = {
    isActiveLike: boolean;
    isActiveDislike: boolean;
    likesCount: number;
    dislikesCount: number;
    like: () => void;
    dislike: () => void;
    className?: string;
}
export const LikesBlock: React.FC<LikesBlockP> = ({
                                                      isActiveLike,
                                                      isActiveDislike,
                                                      like,
                                                      dislike,
                                                      likesCount,
                                                      dislikesCount,
                                                      className,
                                                  }) => {
    const {user} = useContext(AuthContext);

    return <div className={className || styles.likes}>
        <Button
            raised
            text
            disabled={!user}
            severity={isActiveLike && !!user ? "success" : 'info'}
            onClick={like}
            className={classNames(styles.btn)}
            icon="pi pi-thumbs-up-fill"
        >
            <span className={styles.count}>{likesCount}</span>
        </Button>
        <Button
            raised
            text
            disabled={!user}
            severity={isActiveDislike && !!user ? "danger" : 'info'}
            onClick={dislike}
            className={classNames(styles.btn)}
            icon="pi pi-thumbs-down-fill"
        >
            <span className={styles.count}>{dislikesCount}</span>
        </Button>
    </div>
}