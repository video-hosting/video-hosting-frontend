'use client'
import React, {useContext} from "react";
import Link from "next/link";
import dayjs from "@/utils/dayjs";
import {classNames} from "primereact/utils";
import {confirmPopup} from "primereact/confirmpopup";
import {Tag} from "primereact/tag";
import {Button} from "primereact/button";
import {Image} from "primereact/image";
import {IVideoItem} from "@/types/VideoTypes";
import {PreviewVideo} from "@/components/PreviewVideo";
import {api, BASE_URL} from "@/services/api";
import {AuthContext} from "@/components/Provider";
import {useWindowWidth} from "@/hooks";
import styles from './styles.module.css'

type VideoItemP = {
    id: IVideoItem['id'],
    user: IVideoItem['user'],
    tags: IVideoItem['tags'],
    description: IVideoItem['description'],
    duration: IVideoItem['duration'],
    name: IVideoItem['name'],
    views: IVideoItem['views'],
    viewsCount: IVideoItem['viewsCount'],
    createdAt: IVideoItem['createdAt'],
    showUser?: boolean;
    showAvatar?: boolean;
    isMyChannel?: boolean;
    isPrivate?: boolean;
    isAgeRelatedContent: IVideoItem['isAgeRelatedContent'];
    isModerating?: IVideoItem['isModerating'];
    deleteVideo?: (id: number) => void
    editVideo?: (id: number) => void
}

export const VideoItem: React.FC<VideoItemP> = ({
                                                    id,
                                                    user,
                                                    tags,
                                                    description,
                                                    duration,
                                                    name,
                                                    createdAt,
                                                    viewsCount,
                                                    showUser = true,
                                                    showAvatar = true,
                                                    isMyChannel = false,
                                                    isPrivate,
                                                    deleteVideo,
                                                    editVideo,
                                                    isAgeRelatedContent,
                                                    isModerating
                                                }) => {
    const {showError} = useContext(AuthContext)
    const {width} = useWindowWidth()

    const onConfirmDelete = async () => {
        try {
            await api.deleteVideo(id)
            deleteVideo?.(id)
        } catch (err:any) {
            showError('Не удалось удалить видео', err?.response?.data?.message)
        }
    }
    const onDeleteClick: React.MouseEventHandler<HTMLButtonElement> = (event) => {
        confirmPopup({
            target: event.currentTarget,
            message: 'Вы уверены что хотите удалить видео? Восстановление будет невозможно.',
            icon: 'pi pi-info-circle',
            acceptClassName: 'p-button-danger',
            rejectLabel: 'Отменить',
            acceptLabel: 'Да',
            accept: onConfirmDelete,
        });
    };

    return <div className={styles.videoItemContainer}>
        <div className={styles.mainContent}>
            <PreviewVideo id={id} duration={duration}/>
            {showAvatar && <Link className={styles.author} href={`/channel/${user.id}`}>
                <Image
                    imageClassName={styles.avatar}
                    src={`${BASE_URL}/api/avatar/${user?.id}`}
                    alt="avatar"
                    width="44"
                    height="44"
                />
            </Link>}
            <div className={classNames(styles.videoInfo, showAvatar && width > 800 && styles.ellipsis)}>
                <Link className={styles.videoName} href={`/video/${id}`}>{name}</Link>
                <div className={styles.tags}>
                    {isModerating && <Tag severity="info" className={styles.privateVideo} value="Модерация"/>}
                    {isPrivate && <Tag severity="warning" className={styles.privateVideo} value="Приватное видео"/>}
                    {isAgeRelatedContent && <Tag severity="danger" className={styles.privateVideo} value="18+"/>}
                </div>
                <div className={styles.tags}>
                    {tags.map(t => t && <Tag key={t} className={styles.tag} value={t}></Tag>)}
                </div>
                <p className={styles.description}>{description}</p>
                <div className={styles.uploadTime}>
                    <span>Просмотры: {viewsCount}</span>
                    <div className={styles.verticalLine}/>
                    <span>Загружено: {dayjs(createdAt).fromNow()}</span>
                </div>
                {showUser && <p className="flex gap-2 align-items-center">
                    <span className={styles.description}>Автор:</span>
                    <Link className={styles.author} href={`/channel/${user.id}`}>{user.nickname}</Link>
                </p>}
            </div>
        </div>
        {isMyChannel && <div className={styles.videoItemButtons}>
            <Button onClick={() => editVideo?.(id)} text icon="pi pi-pencil"/>
            <Button onClick={onDeleteClick} text icon="pi pi-trash"/>
        </div>}
    </div>
}