'use client'
import React, {createContext, useCallback, useEffect, useRef, useState} from "react";
import {PrimeReactProvider} from "primereact/api";
import {Toast} from "primereact/toast";
import {ProgressBar} from "primereact/progressbar";
import {PageHeader} from "@/components/PageHeader";
import {api} from "@/services/api";
import {IUserFromLogin} from "@/types/UserTypes";
import {authController} from "@/utils/AuthController";
import styles from './styles.module.css'
import {ConfirmPopup} from "primereact/confirmpopup";
import {ThemeE} from "@/types";
import {addAnalytics} from "@/utils/startYaMetric";

type ProviderProps = {
    children: React.ReactNode;
    defaultTheme: ThemeE
}

type AuthContextType = {
    user: IUserFromLogin | null;
    login: (user: IUserFromLogin) => void;
    logout: () => void;
    showError: (summary: string, detail?: string) => void;
    showSuccess: (summary: string, detail?: string) => void;
    showInfo: (summary: string, detail?: string) => void;
    startLoad: () => void;
    stopLoad: () => void;
    isLoadingAuth: boolean,
    setIsLoadingAuth: (v: boolean) => void,
}

export const AuthContext = createContext<AuthContextType>({
    user: null,
    login: () => {},
    logout: () => {},
    showError: () => {},
    showSuccess: () => {},
    showInfo: () => {},
    startLoad: () => {},
    stopLoad: () => {},
    isLoadingAuth: true,
    setIsLoadingAuth: () => {},
});

export const Provider: React.FC<ProviderProps> = ({children, defaultTheme}) => {
    const [user, setUser] = useState<IUserFromLogin | null>(null)
    const [isLoading, setIsLoading] = useState<boolean>(false)
    const [isLoadingAuth, setIsLoadingAuth] = useState<boolean>(true)

    const toast = useRef<Toast>(null);

    const startLoad = useCallback(() => {
        setIsLoading(true)
    },[])
    const stopLoad = useCallback(() => {
        setIsLoading(false)
    }, [])

    const showError = useCallback((summary: string = 'Success', detail?: string) => {
        toast?.current?.show({severity: 'error', summary, detail})
    }, [])

    const showInfo = useCallback((summary: string, detail?:string) => {
        toast?.current?.show({severity: 'info', summary, detail})
    }, [])

    const showSuccess = useCallback((summary: string, detail?:string) => {
        toast?.current?.show({severity: 'success', summary, detail})
    }, [])

    useEffect(() => {
        (async function () {
            try {
                document.addEventListener('mousemove', addAnalytics)
                document.addEventListener('touchstart', addAnalytics)
                const res = await api.signin()
                authController.setAccessToken(res.accessToken)
                setUser(res)
                setIsLoadingAuth(false)
                setInterval(async () => {
                    try {
                        await api.updateAccessToken()
                    } catch (err) {}
                }, 1000 * 60 * 9)
                const cb = async () => {
                    await api.updateAccessToken()
                }
                window.addEventListener('focus', cb)

                addAnalytics()
                return () => window.removeEventListener('focus', cb)
            } catch (err) {
                setIsLoadingAuth(false)
            }
        })()
    }, [])
    return <PrimeReactProvider>
        <AuthContext.Provider value={{
            user,
            logout: () => {
                setUser(null)
                authController.removeAccessToken()
            },
            login: setUser,
            showError,
            showSuccess,
            showInfo,
            startLoad,
            stopLoad,
            isLoadingAuth,
            setIsLoadingAuth
        }}>
            {isLoading && <ProgressBar className={styles.progress} mode="indeterminate"/>}
            <PageHeader defaultTheme={defaultTheme}/>
                <main className={styles.providerContainer}>
                    {children}
                </main>
            <Toast ref={toast}/>
            <ConfirmPopup />
        </AuthContext.Provider>
    </PrimeReactProvider>
}