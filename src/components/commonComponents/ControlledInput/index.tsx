import React from "react";
import {Control, Controller} from "react-hook-form";
import {classNames} from "primereact/utils";
import {InputText} from "primereact/inputtext";

type ControlledInputProps = {
    control: Control<any>;
    fieldName: string;
    disabled?: boolean;
    errorMessage: string | undefined;
    className?: string;
    containerClassName?: string;
    label: string;
    onChange?: (value: string) => void;
    type?: React.ComponentProps<'input'>['type']
    rules: React.ComponentProps<typeof Controller>['rules']
}

export const ControlledInput: React.FC<ControlledInputProps> = ({
                                                                    control,
                                                                    fieldName,
                                                                    errorMessage,
                                                                    rules,
                                                                    disabled,
                                                                    label,
                                                                    onChange,
                                                                    type,
                                                                    className,
                                                                    containerClassName
                                                                }) => {
    return <div className={classNames("flex gap-3 flex-wrap relative", containerClassName)}>
        <Controller
            name={fieldName}
            control={control}
            rules={rules}
            render={({field, fieldState}) => (
                <div style={{width: '100%'}} className="flex flex-column">
                    <span className="p-float-label">
                                <InputText disabled={disabled} type={type} id={field.name} value={field.value}
                                           className={classNames(className, {'p-invalid': fieldState.error})}
                                           onChange={(e) => {
                                               field.onChange(e.target.value)
                                               onChange?.(e.target.value)
                                           }}/>
                                <label htmlFor={field.name}>{label}</label>
                            </span>
                    {errorMessage
                        ? <small className="p-error">{errorMessage}</small>
                        : <small className="p-error">&nbsp;</small>
                    }
                </div>
            )}
        />
    </div>
}