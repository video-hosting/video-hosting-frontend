import React from "react";
import {Control, Controller} from "react-hook-form";
import {classNames} from "primereact/utils";
import {Checkbox} from "primereact/checkbox";

type ControlledCheckboxProps = {
    control: Control<any>;
    fieldName: string;
    className?: string;
    onChange?: (value: boolean, field: any, fieldState:any) => void;
    containerClassName?: string;
    label: string;
}

export const ControlledCheckbox: React.FC<ControlledCheckboxProps> = ({
                                                                          control,
                                                                          fieldName,
                                                                          label,
                                                                          className,
                                                                          containerClassName,
                                                                          onChange,
                                                                      }) => {
    return <div className={classNames("flex gap-3 flex-wrap relative", containerClassName)}>
        <Controller
            name={fieldName}
            control={control}
            render={({field, fieldState}) => (
                <div style={{width: '100%'}} className="flex flex-column align-items-center">
                    <span className="flex cursor-pointer align-items-center">
                        <Checkbox inputId={field.name} checked={field.value} inputRef={field.ref}
                                  className={classNames(className, {'p-invalid mr-1': fieldState.error})}
                                  onChange={(e) => {
                                      field.onChange(e.checked)
                                      onChange?.(!!e.checked, field, fieldState)
                                  }}
                        />
                        <label style={{color: 'var(--secondary-color-text)'}} htmlFor={field.name}
                               className="ml-1 color-p cursor-pointer">{label}</label>
                    </span>
                </div>
            )}
        />
    </div>
}