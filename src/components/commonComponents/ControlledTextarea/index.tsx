import React from "react";
import {Control, Controller} from "react-hook-form";
import { InputTextarea } from "primereact/inputtextarea";
import {classNames} from "primereact/utils";

type ControlledTextareaP = {
    control: Control<any>;
    fieldName: string;
    errorMessage?: string | undefined;
    className?: string;
    containerClassName?: string;
    label: string;
    rules?: React.ComponentProps<typeof Controller>['rules']
}

export const ControlledTextarea:React.FC<ControlledTextareaP> = ({
                                       control,
                                       fieldName,
                                       errorMessage,
                                       rules,
                                       label,
                                       className,
                                       containerClassName,
                                   }) => {
    return <div className={classNames("flex gap-3 flex-wrap relative", containerClassName)}>
        <Controller
            name={fieldName}
            control={control}
            rules={rules}
            render={({field, fieldState}) => (
                <div style={{width: '100%'}} className="flex flex-column">
                    <span className="p-float-label">
                                <InputTextarea autoResize={true} id={field.name} value={field.value}
                                           className={classNames(className, {'p-invalid': fieldState.error})}
                                           onChange={(e) => field.onChange(e.target.value)}/>
                                <label htmlFor={field.name}>{label}</label>
                            </span>
                    {errorMessage
                        ? <small className="p-error">{errorMessage}</small>
                        : <small className="p-error">&nbsp;</small>
                    }
                </div>
            )}
        />
    </div>
}