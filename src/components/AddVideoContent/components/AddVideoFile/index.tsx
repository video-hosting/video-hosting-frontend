import React, {useContext, useState} from "react";
import {useRouter} from "next/navigation";
import {FileUpload, FileUploadHandlerEvent} from "primereact/fileupload";
import {ProgressBar} from "primereact/progressbar";
import {AuthContext} from "@/components/Provider";
import {api} from "@/services/api";
import {InputsType} from "@/components/AddVideoContent/components/AddVideoData";

type AddVideoFileP = {
    videoData: InputsType
}

export const AddVideoFile:React.FC<AddVideoFileP> = ({videoData}) => {
    const {showInfo, showError, stopLoad, startLoad} = useContext(AuthContext);
    const [isUploading, setIsUploading] = useState(false)
    const [progress, setProgress] = useState<number>(0)
    const router = useRouter()

    const onUpload = async (event: FileUploadHandlerEvent) => {
        try {
            startLoad()
            setIsUploading(true)
            setProgress(0)
            const file = event.files[0]
            const formData = new FormData()
            formData.append('file', file)
            formData.append('name', videoData.videoName)
            formData.append('description', videoData.description)
            formData.append('tags', videoData.tags.join(','))
            formData.append('isPrivate', String(videoData.isPrivate))
            formData.append('isAgeRelatedContent', String(videoData.isAgeRelatedContent))
            await api.uploadVideo(formData, (d) => {
                setProgress(Math.ceil((d.progress || 0) * 100))
            })
            stopLoad()
            showInfo('Файл загружен', 'Началась обработка видео, она займет от нескольких ' +
                'минут до нескольких часов. До окончания обработки видео не доступно для просмотра.')
            setIsUploading(false)
            setProgress(0)
            router.push('/')
        } catch (err: any) {
            setIsUploading(false)
            setProgress(0)
            stopLoad()
            showError('Не удалось загрузить видео', err.response.data.message)
        }
    };

    return <div>
        <FileUpload
            disabled={isUploading}
            name="file"
            accept="video/*"
            customUpload
            uploadHandler={onUpload}
        />
        {isUploading && <ProgressBar value={progress}></ProgressBar>}
    </div>
}