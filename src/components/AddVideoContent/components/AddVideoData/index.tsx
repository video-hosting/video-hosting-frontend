import React from "react";
import {classNames} from "primereact/utils";
import {ControlledInput} from "@/components/commonComponents/ControlledInput";
import {ControlledTextarea} from "@/components/commonComponents/ControlledTextarea";
import {ControlledChips} from "@/components/commonComponents/ControlledChips";
import {Button} from "primereact/button";
import {SubmitHandler, useForm} from "react-hook-form";
import {ControlledCheckbox} from "@/components/commonComponents/ControlledCheckbox";
import styles from "./styles.module.css";

export type InputsType = {
    videoName: string;
    description: string;
    isPrivate: boolean;
    isAgeRelatedContent: boolean;
    tags: string[];
}

export const defaultValues: InputsType = {
    videoName: '',
    description: '',
    isPrivate: false,
    isAgeRelatedContent: false,
    tags: [],
}

type AddVideoDataP = {
    saveVideoData: (data: InputsType) => void;
    submit?: SubmitHandler<InputsType>;
    values?: InputsType;
}

export const AddVideoData: React.FC<AddVideoDataP> = ({saveVideoData, values, submit}) => {
    const onSubmit: SubmitHandler<InputsType> = (data) => {
        saveVideoData(data)
    }
    const {
        control,
        handleSubmit,
        formState: {errors},
    } = useForm<InputsType>({defaultValues: values || defaultValues})

    return <form
        className={classNames("card", "flex", "gap-4", styles.content)}
        onSubmit={handleSubmit(submit || onSubmit)}
    >
        <div className={classNames("flex gap-4", styles.row)}>
            <ControlledCheckbox containerClassName={styles.isPrivateCheckbox} control={control} fieldName="isPrivate"
                                label="Приватное видео"/>
            <ControlledCheckbox containerClassName={styles.isPrivateCheckbox} control={control}
                                fieldName="isAgeRelatedContent" label="18+ контент"/>

        </div>
        <div className={classNames("flex gap-4", styles.row)}>
            <ControlledInput
                className={styles.input}
                containerClassName={styles.inputContainer}
                control={control}
                label="Название"
                errorMessage={errors.videoName?.message}
                fieldName="videoName"
                rules={{
                    required: 'Введите название',
                    minLength: {value: 4, message: 'Минимум 4 символа'},
                    maxLength: {value: 100, message: 'Максимум 100 символов'},
                }}
            />
            <ControlledChips
                className={styles.input}
                containerClassName={styles.inputContainer}
                control={control}
                label="Тэги"
                fieldName="tags"
            />
        </div>
        <ControlledTextarea
            className={styles.textarea}
            containerClassName={styles.textareaContainer}
            control={control}
            label="Описание"
            fieldName="description"
        />
        <Button
            type="submit"
            label={!submit ? "Далее" : 'Сохранить'}
        />
    </form>
}