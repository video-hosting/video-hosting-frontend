import React, {useState} from "react";
import {Steps} from "primereact/steps";
import {AddVideoStepsItems} from "@/constants";
import {AddVideoData, defaultValues, InputsType} from "@/components/AddVideoContent/components/AddVideoData";
import {AddVideoFile} from "@/components/AddVideoContent/components/AddVideoFile";
import styles from './styles.module.css'
import {useWindowWidth} from "@/hooks";
import {classNames} from "primereact/utils";

export const AddVideoContent = () => {
    const [activeIndex, setActiveIndex] = useState<number>(0);
    const [videoData, setVideoData] = useState(defaultValues)

    const {width} = useWindowWidth()

    const saveVideoData = (videoData: InputsType) => {
        setActiveIndex(1)
        setVideoData(videoData)
    }

    return <div className={styles.addVideoContent}>
        <Steps
            className={classNames(width < 800 && styles.steps)}
            model={AddVideoStepsItems}
            readOnly
            activeIndex={activeIndex}
        />
        <div className={styles.addVideoContentContainer}>
            {activeIndex === 0 && <AddVideoData saveVideoData={saveVideoData}/>}
            {activeIndex === 1 && <AddVideoFile videoData={videoData} />}
        </div>
    </div>

}