import React, {useContext, useState} from "react";
import {InputTextarea} from "primereact/inputtextarea";
import {Button} from "primereact/button";
import {AuthContext} from "@/components/Provider";
import {api} from "@/services/api";
import {IComment} from "@/types/CommentsTypes";
import {Comment} from "@/components/VideoCommentsBlock/Comment";
import styles from './styles.module.css'

type VideoCommentsBlockP = {
    videoId: string;
    comments: IComment[];
    setComments: (c: IComment[]) => void;
}
export const VideoCommentsBlock:React.FC<VideoCommentsBlockP> = ({videoId, comments, setComments}) => {
    const [comment, setComment] = useState('')
    const [isLoading, setIsLoading] = useState(false)
    const {showError} = useContext(AuthContext);

    const addComment = async () => {
        setIsLoading(true)
        try {
            const res = await api.addComment({
                text: comment
            }, videoId)
            setComments([res.data as IComment, ...comments])
            setComment('')
        } catch (err) {
            showError('Не удалось добавить комментарий')
        }
        setIsLoading(false)
    }

    const onChangeComment = (changedComment: IComment) => {
        setComments(comments.map(c => {
            if (c.id === changedComment.id) {
                return {
                    ...c,
                    ...changedComment
                }
            }
            return c
        }))
    }

    return <div className={styles.container}>
        <span className="p-float-label">
            <InputTextarea
                disabled={isLoading}
                id="video-comment"
                className={styles.textarea}
                placeholder="Добавить комментарий"
                value={comment}
                onChange={(e) => setComment(e.target.value)}
                rows={4}
                cols={30}
            />
            <label htmlFor="video-comment">Добавить комментарий</label>
        </span>
        <Button onClick={addComment} disabled={comment.length === 0 || isLoading} className={styles.addCommentBtn}>Добавить</Button>
        {comments?.map(c => <Comment
            key={c.id}
            onChangeComment={onChangeComment}
            videoId={videoId}
            id={c.id}
            isLiked={c.isLiked}
            isDisliked={c.isDisliked}
            likesCount={c.likesCount}
            dislikesCount={c.dislikesCount}
            childrenCommentsCount={c.childrenCommentsCount}
            createdAt={c.createdAt}
            text={c.text}
            user={c.user}
        />)}
    </div>
}