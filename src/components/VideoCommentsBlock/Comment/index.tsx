import React, {useContext, useRef, useState} from "react";
import {IUserForVideo} from "@/types/UserTypes";
import dayjs from "@/utils/dayjs";
import {Image} from "primereact/image";
import {Button} from "primereact/button";
import {InputTextarea} from "primereact/inputtextarea";
import {AuthContext} from "@/components/Provider";
import {api, BASE_URL} from "@/services/api";
import {IChildrenComment, IComment} from "@/types/CommentsTypes";
import {ChildrenComment} from "@/components/VideoCommentsBlock/Comment/ChildrenComment";
import {LikesBlock} from "@/components/LikesBlock";
import styles from './styles.module.css'

type CommentP = {
    id: number;
    videoId: string;
    createdAt: string;
    text: string;
    childrenCommentsCount: number;
    user: IUserForVideo;
    onChangeComment: (c: IComment) => void;
    isLiked: IComment['isLiked'];
    isDisliked: IComment['isDisliked'];
    likesCount: IComment['likesCount'];
    dislikesCount: IComment['dislikesCount'];
}
export const Comment: React.FC<CommentP> = ({
                                                id,
                                                childrenCommentsCount,
                                                videoId,
                                                createdAt,
                                                user,
                                                text,
                                                onChangeComment,
                                                isLiked,
                                                isDisliked,
                                                likesCount,
                                                dislikesCount,
                                            }) => {
    const [isOpenChildren, setIsOpenChildren] = useState<boolean>(false)
    const [answerValue, setAnswerValue] = useState<string>('')
    const [answerCommentId, setAnswerCommentId] = useState<string | number>(`parent_${id}`)
    const [childrenComments, setChildrenComments] = useState<IChildrenComment[]>([])
    const [isLoading, setIsLoading] = useState(false)
    const {showError, user: authUser} = useContext(AuthContext);
    const textareaRef = useRef<HTMLTextAreaElement | null>(null)
    const addAnswer = async () => {
        setIsLoading(true)
        try {
            const res = await api.addComment({
                text: answerValue,
                answerUserId: answerCommentId === `parent_${id}`
                    ? user.id
                    : childrenComments.find(c => c.id === answerCommentId)?.user.id,
                parentCommentId: id,
                ...(answerCommentId !== `parent_${id}` && {replyId: answerCommentId as number}),
            }, videoId)
            setChildrenComments([...childrenComments, res.data as IChildrenComment])
            setAnswerCommentId(`parent_${id}`)
            setAnswerValue('')
        } catch (err) {
            showError('Не удалось отправить комментарий')
        }
        setIsLoading(false)
    }

    const onAnswer = async () => {
        if (!isOpenChildren) {
            setIsOpenChildren(true)
            const res = await api.getChildrenComments(id)
            setChildrenComments(res.data)
            textareaRef.current?.focus()
            setTimeout(() => {
                textareaRef.current?.scrollIntoView()
            }, 200)
        }
        setAnswerCommentId(`parent_${id}`)
    }

    const openChildren = async () => {
        try {
            setIsOpenChildren(true)
            const res = await api.getChildrenComments(id)
            setChildrenComments(res.data)
        } catch (err) {
            showError('Не удалось загрузить дочерние коментарии')
        }
    }
    const closeChildren = () => {
        setIsOpenChildren(false)
        setChildrenComments([])
    }

    const likeComment = async () => {
        try {
            const {data} = await api.setLikeComment(id)
            onChangeComment({
                id,
                createdAt,
                text,
                user,
                childrenCommentsCount,
                isDisliked: data.isDisliked,
                isLiked: data.isLiked,
                likesCount: data.likes,
                dislikesCount: data.dislikes
            })
        } catch (err) {
            showError('Не удалось поставить лайк')
        }
    }
    const removeLike = async () => {
        try {
            const {data} = await api.deleteLikeComment(id)
            onChangeComment({
                id,
                createdAt,
                text,
                user,
                childrenCommentsCount,
                isDisliked: data.isDisliked,
                isLiked: data.isLiked,
                likesCount: data.likes,
                dislikesCount: data.dislikes
            })
        } catch (err) {
            showError('Не удалось поставить лайк')
        }
    }

    const dislike = async () => {
        try {
            const {data} = await api.setDislikeComment(id)
            onChangeComment({
                id,
                createdAt,
                text,
                user,
                childrenCommentsCount,
                isDisliked: data.isDisliked,
                isLiked: data.isLiked,
                likesCount: data.likes,
                dislikesCount: data.dislikes
            })
        } catch (err) {
            showError('Не удалось поставить лайк')
        }
    }

    const removeDislike = async () => {
        try {
            const {data} = await api.deleteDislikeComment(id)
            onChangeComment({
                id,
                createdAt,
                text,
                user,
                childrenCommentsCount,
                isDisliked: data.isDisliked,
                isLiked: data.isLiked,
                likesCount: data.likes,
                dislikesCount: data.dislikes
            })
        } catch (err) {
            showError('Не удалось поставить лайк')
        }
    }


    return <div className={styles.commentItemContainer}>
        <div className={styles.avatarAuthorTextContainer}>
            <Image
                imageClassName={styles.avatar}
                src={`${BASE_URL}/api/avatar/${user?.id}`}
                alt="avatar"
                width="44"
                height="44"
            />
           <div className={styles.authorText}>
                <div className={styles.author}>
                    {user.nickname} ({dayjs(createdAt).fromNow()})
                    <LikesBlock
                        like={isLiked ? removeLike : likeComment}
                        dislike={isDisliked ? removeDislike : dislike}
                        isActiveLike={isLiked}
                        isActiveDislike={isDisliked}
                        dislikesCount={dislikesCount}
                        likesCount={likesCount}
                        className={styles.likesBlock}
                    />
                </div>
                <p className={styles.commentText}>{text}</p>
            </div>
        </div>
        <div className={styles.btnsContainer}>
            {authUser && <Button
                onClick={onAnswer}
                className={styles.answerBtn}
                text
            >
                Ответить
            </Button>}
            {!!childrenCommentsCount && <Button
                onClick={isOpenChildren ? closeChildren : openChildren}
                className={styles.answerBtnInChild}
                text
            >
                {isOpenChildren ? 'Скрыть' : `Показать ответы (${childrenCommentsCount})`}
            </Button>}
        </div>
        {isOpenChildren && <div className={styles.childrenComments}>
            {childrenComments.map(ch => <ChildrenComment
                key={`${id}__${ch.id}`}
                id={ch.id}
                replyComment={ch.replyId
                    ? childrenComments.find(com => com.id === ch.replyId)
                    : undefined}
                createdAt={ch.createdAt}
                text={ch.text}
                user={ch.user}
                setAnswerId={(id: number) => {
                    setAnswerCommentId(id)
                    setTimeout(() => {
                        textareaRef.current?.focus()
                        textareaRef.current?.scrollIntoView()
                    }, 100)
                }}
            />)}
        </div>}
        {authUser && isOpenChildren && <div className={styles.answerBlock}>
            <p className={styles.author}>
                Ответ на комментарий пользователя {`parent_${id}` === answerCommentId
                ? user.nickname
                : childrenComments.find(c => c.id === answerCommentId)?.user.nickname
            }:
            </p>
            <p className={styles.quotedComment}>
                {`parent_${id}` === answerCommentId
                    ? text
                    : childrenComments.find(c => c.id === answerCommentId)?.text
                }
            </p>
            <InputTextarea
                disabled={isLoading}
                ref={textareaRef}
                id="video-comment"
                className={styles.textarea}
                placeholder="Добавить комментарий"
                value={answerValue}
                onChange={(e) => setAnswerValue(e.target.value)}
                rows={4}
                cols={30}
            />
            <Button onClick={addAnswer} disabled={answerValue.length === 0 || isLoading} className={styles.answerBtnInChild} text>
                Отправить
            </Button>
        </div>}
    </div>
}