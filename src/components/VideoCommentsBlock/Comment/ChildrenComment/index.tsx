import React, {useContext} from "react";
import dayjs from "@/utils/dayjs";
import {Button} from "primereact/button";
import styles from './styles.module.css'
import {IUserForVideo} from "@/types/UserTypes";
import {AuthContext} from "@/components/Provider";
import {BASE_URL} from "@/services/api";
import {Image} from "primereact/image";
import {IChildrenComment} from "@/types/CommentsTypes";

type ChildrenCommentP = {
    id: number;
    text: string;
    createdAt: string;
    user: IUserForVideo;
    setAnswerId: (id: number) => void;
    replyComment: IChildrenComment | undefined;
}

export const ChildrenComment: React.FC<ChildrenCommentP> = ({
                                                                text,
                                                                id,
                                                                user,
                                                                createdAt,
                                                                setAnswerId,
                                                                replyComment,
                                                            }) => {
    const {user: authUser} = useContext(AuthContext);

    return <div className={styles.commentItemContainer}>
        {replyComment && <div className={styles.replyBlock}>
            <i className="pi pi-reply" style={{fontSize: '12px'}}></i>
            <span>{replyComment.user.nickname}: {replyComment.text}</span>
        </div>}
        <div className="flex gap-2">
            <Image
                imageClassName={styles.avatar}
                src={`${BASE_URL}/api/avatar/${user?.id}`}
                alt="avatar"
                width="44"
                height="44"
            />
            <div className="flex flex-column gap-1">
                <p className={styles.author}>{user.nickname} ({dayjs(createdAt).fromNow()})</p>
                <p className={styles.commentText}>{text}</p>
            </div>
        </div>
        {authUser && user.id !== authUser.id && <div className={styles.btnsContainer}>
            <Button
                onClick={() => setAnswerId(id)}
                className={styles.answerBtn}
                text
            >
                Ответить
            </Button>
        </div>}
    </div>
}