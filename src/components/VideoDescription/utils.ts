export const getDescriptionItems = (description: string): string[]=> {
    return description.split('\n');
}