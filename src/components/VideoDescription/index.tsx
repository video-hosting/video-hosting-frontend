import React from "react";
import {getDescriptionItems} from "@/components/VideoDescription/utils";

type VideoDescriptionP = {
    description: string
}

export const VideoDescription: React.FC<VideoDescriptionP> = ({description}) => {
    const items = getDescriptionItems(description)
    return <p>Описание: <br/> {items.map(i => <React.Fragment key={i}>{i}<br/></React.Fragment>)}</p>
}