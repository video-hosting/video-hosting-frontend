import React, {useContext} from "react";
import styles from './styles.module.css'
import {AuthContext} from "@/components/Provider";
import {Button} from "primereact/button";
import {api} from "@/services/api";
import {IVideoWithUserLikes} from "@/types/VideoTypes";

type SubscribeBlockP = {
    userId: number;
    userNickname: string;
    subscribersCount: number;
    isOwnVideo: boolean;
    setSubscribersCount: (count: number) => void
}

export const SubscribeBlock: React.FC<SubscribeBlockP> = ({
                                                              userId,
                                                              userNickname,
                                                              isOwnVideo,
                                                              setSubscribersCount,
                                                              subscribersCount,
}) => {
    const {showError, user, login} = useContext(AuthContext);

    const isSubscribedThisUser = user?.subscribes.some(s => s.id === userId)

    const subscribe = async () => {
        try {
            await api.subscribe(userId)
            if (user) {
                login({...user, subscribes: [{id: userId, nickname: userNickname}, ...user?.subscribes]})
            }
            setSubscribersCount(subscribersCount + 1)
        } catch (err) {
            showError('Не удалось подписаться')
        }
    }

    const unsubscribe = async () => {
        try {
            await api.unsubscribe(userId)
            if (user) {
                login({...user, subscribes: user.subscribes.filter(s => s.id !== userId)})
            }
            setSubscribersCount(subscribersCount - 1)
        } catch (err) {
            showError('Не удалось отписаться')
        }
    }

    return <div className={styles.subscribeBlock}>
        <span className={styles.count}>Подписчики: {subscribersCount}</span>
        {user && !isOwnVideo && <div className={styles.verticalLine}/>}
        {user && !isOwnVideo && <Button
            text
            className={styles.btn}
            onClick={isSubscribedThisUser ? unsubscribe : subscribe}
        >
            {isSubscribedThisUser ? 'Отписаться' : 'Подписаться'}
        </Button>}
    </div>
}