import {Metadata} from "next";
import VideoPage from './page';
import {api, BASE_URL_FOR_NEXT_SERVER} from "@/services/api";
import React from "react";
import {IUserForVideo} from "@/types/UserTypes";
import {IVideoItem, IVideoWithUserLikes} from "@/types/VideoTypes";

export const generateMetadata = async ({ params }: any):Promise<Metadata> => {
    const res = await api.getVideoDataServer(params.id).catch(() => null)
    return {
        title: `${res?.data.name} - Видео-хостинг`,
        description: `${res?.data.description}. Сервис видеохостинга с добавлением и просмотром видео, с возможность подписок и комментирования`,
        openGraph: {
            title: res?.data.name,
            description: res?.data.description,
            type: 'website',
            images: {url: `https://video-hosting.site/api/video/preview-image/${params.id}`}
        }
    }
}

async function getVideoData(id: string):Promise<IVideoWithUserLikes | null> {
    const res = await fetch(`${BASE_URL_FOR_NEXT_SERVER}/api/video/${id}`, {
        cache: 'no-store'
    })

    if (!res.ok) {return null}

    return res.json()
}

async function getRelatedVideos(id: string):Promise<IVideoItem[]> {
    const res = await fetch(`${BASE_URL_FOR_NEXT_SERVER}/api/video/related/${id}`, {
        cache: 'no-store'
    })

    if (!res.ok) {return []}

    return res.json()
}

type VideoPageLayoutP = {
    params: {
        id: string
    }
}

const VideoPageLayout:React.FC<VideoPageLayoutP> = async ({params}) => {
    const videoData = await getVideoData(params.id)
    const relatedVideos = await getRelatedVideos(params.id)

    return <VideoPage params={{...params, defaultData: videoData, defaultVideos: relatedVideos}}/>
}

export default VideoPageLayout