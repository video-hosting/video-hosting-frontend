'use client'
import React, {useContext, useState, useEffect} from "react";
import dayjs from "@/utils/dayjs";
import {Image} from "primereact/image";
import {Tag} from "primereact/tag";
import {classNames} from "primereact/utils";
import {AuthContext} from "@/components/Provider";
import {api, BASE_URL} from "@/services/api";
import {IVideoItem, IVideoWithUserLikes} from "@/types/VideoTypes";
import {LikesBlock} from "@/components/LikesBlock";
import {VideoCommentsBlock} from "@/components/VideoCommentsBlock";
import {IComment} from "@/types/CommentsTypes";
import Link from "next/link";
import {SubscribeBlock} from "@/components/SubscribeBlock";
import {VideoPlayer} from "@/components/VideoPlayer";
import {useWindowWidth} from "@/hooks";
import {VideoItem} from "@/components/VideoItem";
import {VideoDescription} from "@/components/VideoDescription";
import styles from './page.module.css'

type VideoPageP = {
    params: {
        id: string;
        defaultData:IVideoWithUserLikes| null;
        defaultVideos: IVideoItem[];
    }
}

export default function VideoPage({params}: VideoPageP) {
    const [videoData, setVideoData] = useState<IVideoWithUserLikes | null>(params.defaultData)
    const [relatedVideos, setRelatedVideos] = useState<IVideoItem[]>(params.defaultVideos)
    const {showError, user} = useContext(AuthContext);
    const {width} = useWindowWidth()
    const isOwnVideo = !!(user && videoData && user.id === videoData.user.id)

    const like = async () => {
        try {
            const {data} = await api.setLike(params.id)
            if (videoData) {
                setVideoData({
                    ...videoData,
                    isDislikedVideo: data.isDisliked,
                    isLikedVideo: data.isLiked,
                    likes: data.likes,
                    dislikes: data.dislikes
                })
            }
        } catch (err) {
            showError('Не удалось поставить лайк')
        }
    }
    const removeLike = async () => {
        try {
            const {data} = await api.deleteLike(params.id)
            if (videoData) {
                setVideoData({
                    ...videoData,
                    isDislikedVideo: data.isDisliked,
                    isLikedVideo: data.isLiked,
                    likes: data.likes,
                    dislikes: data.dislikes
                })
            }
        } catch (err) {
            showError('Не удалось поставить лайк')
        }
    }

    const dislike = async () => {
        try {
            const {data} = await api.setDislike(params.id)
            if (videoData) {
                setVideoData({
                    ...videoData,
                    isDislikedVideo: data.isDisliked,
                    isLikedVideo: data.isLiked,
                    likes: data.likes,
                    dislikes: data.dislikes
                })
            }
        } catch (err) {
            showError('Не удалось поставить лайк')
        }
    }

    const removeDislike = async () => {
        try {
            const {data} = await api.deleteDislike(params.id)
            if (videoData) {
                setVideoData({
                    ...videoData,
                    isDislikedVideo: data.isDisliked,
                    isLikedVideo: data.isLiked,
                    likes: data.likes,
                    dislikes: data.dislikes
                })
            }
        } catch (err) {
            showError('Не удалось поставить лайк')
        }
    }

    const setComments = (comments: IComment[]) => {
        setVideoData({...videoData, comments} as IVideoWithUserLikes)
    }

    const setSubscribersCount = (count: number) => {
        if (videoData) {
            setVideoData({...videoData, user: {...videoData.user, subscribersCount: count}})
        }
    }

    useEffect(() => {
        (async function () {
            const res = await api.getVideoData(params.id)
            setVideoData(res.data)
            const res2 = await api.getRelatedVideo(params.id)
            setRelatedVideos(res2.data)
        })()
    }, [])

    return (
        <div className={styles.page} draggable={false}>
            <div className={styles.mainPart}>
                {videoData && <h1 className={styles.title}>{videoData.name}</h1>}
                {videoData && <VideoPlayer videoId={params.id} duration={videoData.duration}/>}
                {videoData &&
                    <div className={classNames("flex flex-column gap-2 align-self-start", styles.descriptionPanel)}>
                        <div className={classNames("flex align-items-center gap-2", styles.likesAndAuthor)}>
                            <div className="flex align-items-center gap-2 flex-wrap">
                                <Link href={`/channel/${videoData.user.id}`}>
                                    <Image
                                        imageClassName={styles.avatar}
                                        src={`${BASE_URL}/api/avatar/${videoData.user.id}`}
                                        alt="avatar"
                                        width="44"
                                        height="44"
                                    />
                                </Link>
                                <Link href={`/channel/${videoData.user.id}`}>{videoData.user.nickname}</Link>
                                <SubscribeBlock
                                    isOwnVideo={isOwnVideo}
                                    setSubscribersCount={setSubscribersCount}
                                    userId={videoData.user.id}
                                    userNickname={videoData.user.nickname}
                                    subscribersCount={videoData.user.subscribersCount}
                                />
                                <p>Просмотры: {videoData.viewsCount}</p>
                            </div>
                            <LikesBlock
                                like={videoData.isLikedVideo ? removeLike : like}
                                dislike={videoData.isDislikedVideo ? removeDislike : dislike}
                                isActiveLike={!!videoData.isLikedVideo}
                                isActiveDislike={!!videoData.isDislikedVideo}
                                dislikesCount={videoData.dislikes}
                                likesCount={videoData.likes}
                            />
                        </div>
                        <div className={styles.videoContentContainer}>
                            <div className="flex gap-4">
                                Загружено: {dayjs(videoData.createdAt).fromNow()}
                                <div className={styles.tags}>
                                    {videoData.tags.map(t => t && <Tag key={t} className={styles.tag} value={t}></Tag>)}
                                </div>
                            </div>
                            {videoData.description && <VideoDescription description={videoData.description}/>}
                        </div>
                    </div>}
                {videoData && <VideoCommentsBlock
                    setComments={setComments}
                    comments={videoData.comments}
                    videoId={params.id}
                />}
            </div>
            {width > 900 && <div className={styles.relatedVideosPart}>
                {videoData && <div className="flex flex-column gap-4">
                    {relatedVideos.map(v => <VideoItem
                        key={v.id}
                        views={v.views}
                        id={v.id}
                        isAgeRelatedContent={v.isAgeRelatedContent}
                        isModerating={v.isModerating}
                        duration={v.duration}
                        viewsCount={v.viewsCount}
                        name={v.name}
                        description={v.description}
                        tags={v.tags}
                        createdAt={v.createdAt}
                        showAvatar={false}
                        showUser={false}
                        user={videoData.user}
                    />)}
                </div>}
            </div>}
        </div>
    )
}
