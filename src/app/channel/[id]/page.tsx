'use client'
import React, {useContext, useState} from "react";
import {AuthContext} from "@/components/Provider";
import {api, BASE_URL} from "@/services/api";
import {IUserForVideo} from "@/types/UserTypes";
import {VideoItem} from "@/components/VideoItem";
import {Image} from "primereact/image";
import {IVideoItem} from "@/types/VideoTypes";
import {EditVideoModal} from "@/components/EditVideoModal";
import {InputsType} from "@/components/AddVideoContent/components/AddVideoData";
import {SubscribeBlock} from "@/components/SubscribeBlock";
import styles from './page.module.css'

type ChannelPageP = {
    params: {
        id: string,
        defaultChannelInfo: (IUserForVideo & {subscribersCount: number}) | null
    },
}

export default function ChannelPage({
                                        params,
}: ChannelPageP) {
    const [channelInfo, setChannelInfo] = useState<IUserForVideo & {subscribersCount: number} | null>(params.defaultChannelInfo)
    const [isOpenEditModal, setIsOpenEditModel] = useState(false)
    const [editVideoData, setEditVideoData] = useState<IVideoItem | null>(null)
    const {showError, user} = useContext(AuthContext);
    const isMyChannel = +params.id === user?.id


    const deleteVideo = (videoId: number) => {
        setChannelInfo((prev) => {
            if (!prev) return prev
            return {
                ...channelInfo,
                videos: channelInfo?.videos?.filter(v => v.id !== videoId)
            } as IUserForVideo & {subscribersCount: number}
        })
    }

    const openModalEditVideo = (videoId: number) => {
        if (channelInfo) {
            const currentVideo = channelInfo.videos?.find(v => v.id === videoId)
            if (currentVideo) {
                setEditVideoData(currentVideo);
                setIsOpenEditModel(true);
            }
        }
    }

    const onEditSubmit = async (data: InputsType) => {
        try {
            if (editVideoData) {
                await api.updateVideo(editVideoData.id, {
                    ...data,
                    tags: data.tags.join(',')
                })
                const currentVideo = channelInfo?.videos?.find(v => v.id === editVideoData.id)
                if (currentVideo) {
                    setChannelInfo((prev) => {
                        if (!prev) return prev
                        return {
                            ...channelInfo,
                            videos: channelInfo?.videos?.map(v => {
                                if (v.id === editVideoData.id) {
                                    return {
                                        ...v,
                                        name: data.videoName,
                                        description: data.description,
                                        isPrivate: data.isPrivate,
                                        tags: data.tags,
                                    }
                                }
                                return v
                            })
                        } as IUserForVideo & {subscribersCount: number}
                    })
                }

                setEditVideoData(null)
                setIsOpenEditModel(false)
            }
        } catch (err: any) {
            showError('Не удалось изменить данные видео', err.response.data.message)
        }
    }

    const onCancel = () => {
        setEditVideoData(null)
        setIsOpenEditModel(false)
    }

    const setSubscribersCount = (count: number) => {
        if (channelInfo) {
            setChannelInfo({...channelInfo, subscribersCount: count})
        }
    }

    return (
        <div className={styles.page}>
            <div className={styles.titleContainer}>
                <Image
                    imageClassName={styles.avatar}
                    src={`${BASE_URL}/api/avatar/${params.id}`}
                    alt="avatar"
                    width="44"
                    height="44"
                />
                <h3 className={styles.title}>{channelInfo?.nickname}</h3>
            </div>
            {channelInfo && <SubscribeBlock
                isOwnVideo={isMyChannel}
                setSubscribersCount={setSubscribersCount}
                userId={channelInfo.id}
                userNickname={channelInfo.nickname}
                subscribersCount={channelInfo.subscribersCount}
            />}
            {channelInfo && <div className="flex flex-column gap-4">
                {channelInfo?.videos?.map(v => <VideoItem
                    key={v.id}
                    id={v.id}
                    viewsCount={v.viewsCount}
                    views={v.views}
                    duration={v.duration}
                    name={v.name}
                    description={v.description}
                    tags={v.tags}
                    createdAt={v.createdAt}
                    user={channelInfo}
                    showUser={false}
                    showAvatar={false}
                    isPrivate={v.isPrivate}
                    isAgeRelatedContent={v.isAgeRelatedContent}
                    isModerating={v.isModerating}
                    isMyChannel={isMyChannel}
                    deleteVideo={deleteVideo}
                    editVideo={openModalEditVideo}
                />)}
            </div>}
            <EditVideoModal
                visible={isOpenEditModal}
                videoData={editVideoData}
                onEdit={onEditSubmit}
                onCancel={onCancel}
            />
        </div>
    )
}
