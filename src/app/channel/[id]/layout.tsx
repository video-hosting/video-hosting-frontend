import {Metadata} from "next";
import ChannelPage from './page';
import {api, BASE_URL_FOR_NEXT_SERVER} from "@/services/api";
import React from "react";
import {IUserForVideo} from "@/types/UserTypes";

export const generateMetadata = async ({ params }: any):Promise<Metadata> => {
    const res = await api.getChannelInfoServer(params.id).catch(() => null)
    return {
        title: `Канал ${res?.data.nickname}`,
        description: `Канал пользователя ${res?.data.nickname}`,
        openGraph: {
            title: res?.data.nickname,
            description: `Канал пользователя ${res?.data.nickname}`,
            type: 'website',
            images: {url: `https://video-hosting.site/api/avatar/${params.id}`}
        }
    }
}

async function getData(id: string):Promise<IUserForVideo & {subscribersCount: number} | null> {
    const res = await fetch(`${BASE_URL_FOR_NEXT_SERVER}/api/channel/${id}`, {
        cache: 'no-store'
    })

    if (!res.ok) {return null}

    return res.json()
}

type LayoutChannelPageP = {
    params: {
        id:string
    }
}

const LayoutChannelPage:React.FC<LayoutChannelPageP> = async ({params}) => {
    const channelInfo = await getData(params.id)
    return <>
        <ChannelPage params={{...params, defaultChannelInfo: channelInfo}} />
    </>
}

export default LayoutChannelPage