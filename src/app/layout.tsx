import React from "react";
import type {Metadata} from 'next'
import {Inter} from 'next/font/google'
import {Provider} from "@/components/Provider";
import "primereact/resources/primereact.min.css";
import 'primeicons/primeicons.css';
import 'primeflex/primeflex.css'
import './globals.css'

import {APP_THEME_ID} from "@/constants";
import {cookies} from "next/headers";
import {ThemeE} from "@/types";

const inter = Inter({subsets: ['latin']})

export const metadata: Metadata = {
    title: 'Видео хостинг',
    description: 'Сервис видеохостинга с добавлением и просмотром видео, с возможность подписок и комментирования',
    robots: 'all',
    keywords: 'видео, хостинг, смотреть видео, онлайн',
    authors: [{name: 'Алексей Рыжов fristail27@gmail.com', url: 'https://gitlab.com/fristail27'}],
    openGraph: {
        title: 'Видео хостинг',
        description: 'Сервис видеохостинга с добавлением и просмотром видео, с возможность подписок и комментирования',
        type: 'website',
        url: 'https://video-hosting.site',
        images: {
            url: 'https://video-hosting.site/meta/android-chrome-192x192.png',
        }
    }
}

type RootLayoutProps = {
    children: React.ReactNode
}

const RootLayout: React.FC<RootLayoutProps> = ({children}) => {
    const cookieStore = cookies()
    const defaultTheme = cookieStore.get('theme')?.value || ThemeE.light
    return <html lang="en">
    <head>
        <link id={APP_THEME_ID} rel="stylesheet" href={`/themes/${defaultTheme}/theme.css`}/>
        <link rel="apple-touch-icon" sizes="180x180" href="/meta/apple-touch-icon.png"/>
        <link rel="icon" type="image/png" sizes="32x32" href="/meta/favicon-32x32.png"/>
        <link rel="icon" type="image/png" sizes="16x16" href="/meta/favicon-16x16.png"/>
        <link rel="manifest" href="/meta/site.webmanifest"/>
        <link rel="mask-icon" href="/meta/safari-pinned-tab.svg" color="#5bbad5"/>
        <meta name="msapplication-TileColor" content="#da532c"/>
        <meta name="theme-color" content="#ffffff"/>
        <meta name="yandex" content="all"/>
    </head>
    <body className={inter.className}>
    <Provider defaultTheme={defaultTheme as ThemeE}>
        {children}
    </Provider>
    </body>
    </html>
}


export default RootLayout