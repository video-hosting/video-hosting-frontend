'use client'
import {useEffect, useState} from "react";
import {IVideoItem} from "@/types/VideoTypes";
import {VideoItem} from "@/components/VideoItem";
import {api} from "@/services/api";
import styles from './page.module.css'

type SearchResultsPageP = {
    searchParams: { q: string }
}

export default function SearchResultsPage({searchParams}: SearchResultsPageP) {
    const [videos, setVideos] = useState<IVideoItem[]>([])
    useEffect(() => {
        (async function () {
            const res = await api.getVideos(searchParams.q)
            setVideos(res.data)
        })()
    }, [searchParams.q])

    return (
        <div className={styles.page}>
            <span>Результаты по поиску: <span className={styles.searchResult}>{searchParams.q}</span></span>
            <div className="flex flex-column gap-4">
                {videos.map(v => <VideoItem
                    key={v.id}
                    views={v.views}
                    isAgeRelatedContent={v.isAgeRelatedContent}
                    isModerating={v.isModerating}
                    id={v.id}
                    duration={v.duration}
                    viewsCount={v.viewsCount}
                    name={v.name}
                    description={v.description}
                    tags={v.tags}
                    createdAt={v.createdAt}
                    user={v.user}
                />)}
            </div>
        </div>
    )
}
