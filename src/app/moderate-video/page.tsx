'use client'
import React, {useContext, useEffect, useState} from "react";
import Link from "next/link";
import {Checkbox, CheckboxChangeEvent} from "primereact/checkbox";
import {Button} from "primereact/button";
import {IVideoItem} from "@/types/VideoTypes";
import {AuthContext} from "@/components/Provider";
import {api} from "@/services/api";
import styles from './page.module.css'

export default function ModerateVideoPage() {
    const [videos, setVideos] = useState<IVideoItem[]>([])
    const {showError} = useContext(AuthContext)
    const onCheck = async (id: number, e: CheckboxChangeEvent) => {
        try {
            const {data} = await api.setAgeControl(id, !!e.checked)
            setVideos(videos.map(v => {
                if (v.id === id) {
                    return {...v, isAgeRelatedContent: data}
                }
                return v
            }))
        } catch (err:any) {
            showError('не удалось выполнить запрос', err.response.data.message)
        }
    }

    const approveVideo = async (id: number) => {
        try {
            await api.approveVideo(id)
            setVideos(videos.filter(v => v.id !== id))
        } catch (err:any) {
            showError('не удалось выполнить запрос', err.response.data.message)
        }
    }

    useEffect(() => {
        (async function () {
            const res = await api.getModerateVideos()
            setVideos(res.data)
        })()
    }, [])

    return (
        <div className={styles.page}>
            {videos.map(v => <div key={v.id} className="flex gap-4 align-items-center">
                <Link href={`/video/${v.id}`}>{v.name}</Link>
                <span className="flex cursor-pointer align-items-center">
                        <Checkbox
                            id={v.id.toString()}
                            checked={!!v.isAgeRelatedContent}
                            onChange={(e) => onCheck(v.id, e)}
                        />
                        <label style={{color: 'var(--secondary-color-text)'}} htmlFor={v.id.toString()}
                               className="ml-1 color-p cursor-pointer">18+</label>
                    </span>
                <Button onClick={() => approveVideo(v.id)} severity="success" size="small">Одобрить</Button>
            </div>)}
        </div>
    )
}
