'use client'
import React from "react";
import {AddVideoContent} from "@/components/AddVideoContent";
import styles from './page.module.css'

export default function AddVideoPage() {
    return (
        <div className={styles.addVideMain}>
            <h1 className={styles.title}>Добавить видео</h1>
            <AddVideoContent/>
        </div>
    )
}
