import {VideoItem} from "@/components/VideoItem";
import {BASE_URL_FOR_NEXT_SERVER} from "@/services/api";
import styles from './page.module.css'
import {IVideoItem} from "@/types/VideoTypes";

async function getData():Promise<IVideoItem[]> {
    const res = await fetch(`${BASE_URL_FOR_NEXT_SERVER}/api/video/list`, {
        cache: 'no-store'
    })

    if (!res.ok) {return []}

    return res.json()
}

export default async function Home() {
    const videos = await getData()
    return (
        <main className={styles.main}>
            <div className="flex flex-column gap-4">
                {videos.map(v => <VideoItem
                    key={v.id}
                    views={v.views}
                    id={v.id}
                    duration={v.duration}
                    viewsCount={v.viewsCount}
                    name={v.name}
                    description={v.description}
                    isAgeRelatedContent={v.isAgeRelatedContent}
                    isModerating={v.isModerating}
                    tags={v.tags}
                    createdAt={v.createdAt}
                    user={v.user}
                />)}
            </div>
        </main>
    )
}
