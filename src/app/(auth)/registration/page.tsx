'use client'
import React, {useContext, useState} from "react";
import {useRouter} from "next/navigation";
import {useForm, SubmitHandler} from "react-hook-form"
import md5 from "md5";
import {Button} from "primereact/button";
import {ControlledInput} from "@/components/commonComponents/ControlledInput";
import {api} from "@/services/api";
import {authController} from "@/utils/AuthController";
import {AuthContext} from "@/components/Provider";
import {ControlledCheckbox} from "@/components/commonComponents/ControlledCheckbox";
import {classNames} from "primereact/utils";
import styles from './page.module.css'

type Inputs = {
    login: string
    email: string
    name: string
    surname: string
    nickname: string
    password: string
    'repeat-password': string
}

const defaultValues = {
    login: '',
    email: '',
    name: '',
    surname: '',
    nickname: '',
    password: '',
    "repeat-password": '',
}

export default function RegistrationPage() {
    const {
        control,
        setValue,
        getValues,
        handleSubmit,
        formState: {errors},
    } = useForm<Inputs>({defaultValues})
    const router = useRouter()

    const {login, showError, showSuccess, showInfo} = useContext(AuthContext);
    const [isDisabledNick, setIsDisabledNick] = useState(false)
    const onCheckWithoutNick = (value: boolean) => {
        if (value) {
            setValue("nickname", getValues().login)
            setIsDisabledNick(true)
        } else {
            setValue('nickname', '')
            setIsDisabledNick(false)
        }
    }

    const onChangeLogin = (value: string) => {
        if (isDisabledNick) {
            setValue('nickname', value)
        }
    }

    const onSubmit: SubmitHandler<Inputs> = async (data) => {
        try {
            const {accessToken, ...user} = await api.signup({
                ...data,
                password: undefined,
                "repeat-password": undefined,
                passwordHash: md5(data.password)
            })
            authController.setAccessToken(accessToken)
            login(user)
            setInterval(async () => {
                try {
                    await api.updateAccessToken()
                } catch (err) {}
            }, 1000 * 60 * 9)
            showSuccess('Вы успешно зарегестрировались')
            showInfo('Подтвердите аккаунт.', 'на вашу почту отправлено письмо для подтверждения аккаунта.')
            router.push('/')
        } catch (err: any) {
            showError('не удалось зарегестрироваться', err.response.data.message)
        }
    }
    return (
        <div className={styles.addVideMain}>
            <h1>Регистрация</h1>
            <form
                onSubmit={handleSubmit(onSubmit)}
                className={classNames(styles.container ,"card flex flex-column gap-4")}
            >
                <div className="flex gap-3 flex-wrap">
                    <ControlledInput
                        control={control}
                        label="Логин"
                        errorMessage={errors.login?.message as string}
                        fieldName="login"
                        onChange={onChangeLogin}
                        rules={{
                            required: 'Введите логин',
                            minLength: {value: 4, message: 'Минимум 4 символа'},
                            maxLength: {value: 20, message: 'Максимум 20 символов'},
                            validate: (value: string) => {
                                return !value.includes('@') ? true : 'Логин не может содержать спецсимволы'
                            }
                        }}
                    />
                    <ControlledInput
                        control={control}
                        label="E-mail"
                        errorMessage={errors.email?.message}
                        type="email"
                        fieldName="email"
                        rules={{
                            required: 'Введите E-mail',
                            pattern: {
                                value: /\S+@\S+\.\S+/,
                                message: "Введите корректный e-mail",
                            },
                        }}
                    />
                </div>
                <div className="flex gap-3 flex-wrap">
                    <ControlledInput
                        control={control}
                        label="Имя"
                        errorMessage={errors.name?.message}
                        fieldName="name"
                        rules={{
                            required: 'Введите имя',
                            maxLength: {value: 20, message: 'Максимум 20 символов'},
                        }}
                    />
                    <ControlledInput
                        control={control}
                        label="Фамилия"
                        errorMessage={errors.surname?.message}
                        fieldName="surname"
                        rules={{
                            required: 'Введите фамилию',
                            maxLength: {value: 20, message: 'Максимум 20 символов'},
                        }}
                    />
                </div>
                <div className="flex gap-3 flex-wrap align-items-center">
                    <ControlledInput
                        control={control}
                        disabled={isDisabledNick}
                        label="Ник"
                        errorMessage={errors.name?.message}
                        fieldName="nickname"
                        rules={{
                            required: 'Введите ник',
                            maxLength: {value: 16, message: 'Максимум 16 символов'},
                        }}
                    />
                    <ControlledCheckbox
                        onChange={onCheckWithoutNick}
                        control={control}
                        containerClassName={styles.checkboxContainer}
                        label="Логин вместо ника"
                        fieldName="nameToNick"
                    />
                </div>
                <div className="flex gap-3 flex-wrap">
                    <ControlledInput
                        control={control}
                        label="Пароль"
                        errorMessage={errors.password?.message}
                        fieldName="password"
                        type="password"
                        rules={{
                            required: 'Введите пароль',
                            minLength: {value: 10, message: 'Минимум 10 символов'},
                            maxLength: {value: 20, message: 'Максимум 20 символов'},
                        }}
                    />
                    <ControlledInput
                        control={control}
                        label="Повторите пароль"
                        errorMessage={errors["repeat-password"]?.message}
                        fieldName="repeat-password"
                        type="password"
                        rules={{
                            required: 'Введите пароль',
                            minLength: {value: 10, message: 'Минимум 10 символов'},
                            maxLength: {value: 20, message: 'Максимум 20 символов'},
                            validate: (value, formValues) => {
                                return formValues.password === value ? true : 'Пароль не соответсвует'
                            }
                        }}
                    />
                </div>
                <Button
                    type="submit"
                    label="Зарегестрироваться"
                    icon="pi pi-user"
                />
            </form>
        </div>
    )
}
