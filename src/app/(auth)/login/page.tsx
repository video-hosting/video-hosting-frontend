'use client'
import React, {useContext, useState} from "react";
import {useRouter} from "next/navigation";
import Link from "next/link";
import md5 from 'md5'
import {InputText} from "primereact/inputtext";
import {Button} from "primereact/button";
import {api} from "@/services/api";
import {AuthContext} from "@/components/Provider";
import {authController} from "@/utils/AuthController";
import styles from './page.module.css'

export default function LoginPage() {
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');
    const {login: loginCb, showError} = useContext(AuthContext)
    const router = useRouter()

    const loginHandler = async () => {
        try {
            const res = await api.signin({login, passwordHash: md5(password)})
            if (res) {
                loginCb(res)
                authController.setAccessToken(res.accessToken)
                router.push('/')
            }
        } catch (err: any) {
            showError('Ошибка авторизации', err.response.data.message)
        }
    }
    return (
        <div className={styles.addVideMain}>
            <h1 className={styles.title}>Войти</h1>
            <div className={styles.formContainer}>
                <div className="flex flex-column gap-2">
                    <span className="p-input-icon-left p-float-label">
                        <i className="pi pi-user"/>
                        <InputText
                            value={login}
                            onChange={(e) => setLogin(e.target.value)}
                            id="username"
                            type="text"
                            placeholder="Логин или Email"
                        />
                        <label htmlFor="username">Логин или Email</label>
                    </span>
                    <Link href="/registration"><small>Создать аккаунт</small> </Link>
                </div>
                <div className="flex flex-column gap-2">
                    <span className="p-input-icon-left p-float-label">
                        <i className="pi pi-key"/>
                        <InputText
                            value={password}
                            onChange={(e) => setPassword(e.target.value)}
                            type="password"
                            id="password"
                            placeholder="Пароль"
                            aria-describedby="username-help"
                        />
                        <label htmlFor="password">Пароль</label>
                    </span>
                    <Link href="/reset-password">
                        <small id="password">
                            Восстановить пароль
                        </small>
                    </Link>
                </div>
            </div>
            <Button className={styles.loginBtn} onClick={loginHandler} label="Войти"/>
        </div>
    )
}
