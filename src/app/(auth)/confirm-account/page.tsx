'use client'
import React, {useContext, useEffect} from "react";
import {useRouter, useSearchParams} from "next/navigation";
import {api} from "@/services/api";
import {AuthContext} from "@/components/Provider";

export default function ConfirmAccountPage() {
    const router = useRouter()
    const params = useSearchParams()
    const key = params.get('key')

    const {showError} = useContext(AuthContext);

    useEffect(() => {
        (async function () {
            try {
                await api.confirmAccount(key as string)
                router.push('/')
            } catch (err: any) {
                showError('Не удалось подтвердить аккаунт', err.response.data.message)
            }
        })()
    }, [])

    return (
        <p>Подтверждение аккаунта</p>
    )
}
