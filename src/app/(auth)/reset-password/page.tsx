'use client'
import React, {useContext, useState} from "react";
import {InputText} from "primereact/inputtext";
import {Button} from "primereact/button";
import {api} from "@/services/api";
import styles from './page.module.css'
import {AuthContext} from "@/components/Provider";
import {useRouter} from "next/navigation";

export default function ResetPasswordPage() {
    const [login, setLogin] = useState('')

    const {showError, startLoad, stopLoad, showSuccess} = useContext(AuthContext);
    const router = useRouter()

    const resetPassword = async () => {
        try {
            startLoad()
            await api.resetPassword(login)
            showSuccess('Отправлен запрос на восстановление пароля',
                'на вашу почту отправлено письмо для восстановления пароля, следуйте инструкциям в письме')
            router.push('/')
        } catch (err: any) {
            showError('не удалось выполнить запрос', err.response.data.message)
        }
        stopLoad()
    }
    return (
        <div className={styles.resetPasswordPage}>
            <h1 className={styles.title}>Восстановить пароль</h1>
            <div className={styles.formContainer}>
                <span className="p-input-icon-left p-float-label">
                    <i className="pi pi-user"/>
                    <InputText
                        className={styles.input}
                        value={login}
                        onChange={(e) => setLogin(e.target.value)}
                        id="username"
                        type="text"
                        placeholder="Логин или Email"
                    />
                    <label htmlFor="username">Введите Логин или Email</label>
                    </span>
                <Button
                    onClick={resetPassword}
                    label="Восстановить пароль"
                />
            </div>
        </div>
    )
}
