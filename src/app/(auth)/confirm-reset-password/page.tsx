'use client'
import {InputText} from "primereact/inputtext";
import {useRouter, useSearchParams} from "next/navigation";
import React, {useContext, useEffect, useState} from "react";
import md5 from "md5";
import {Button} from "primereact/button";
import {api} from "@/services/api";
import {AuthContext} from "@/components/Provider";
import styles from './page.module.css'

export default function ConfirmResetPasswordPage() {
    const [pass, setPass] = useState('')
    const [repeatPass, setRepeatPass] = useState('')
    const [isAvailableUpdate, setIsAvailableUpdate] = useState(false)

    const {showError, startLoad, stopLoad, showSuccess} = useContext(AuthContext);
    const router = useRouter()
    const params = useSearchParams()
    const key = params.get('key')

    const updatePassword = async () => {
        try {
            startLoad()
            await api.updatePassword(md5(pass), key as string)
            stopLoad()
            router.push('/login')
            showSuccess('Пароль успешно изменен', 'Авторизуйтесь с новым паролем.')
        } catch (err:any) {
            stopLoad()
            showError('Ссылка недействительна', err.response.data.message)
        }
    }

    useEffect(() => {
        (async function () {
            try {
                await api.checkLinkResetPassword(key as string)
                setIsAvailableUpdate(true)
            } catch (err) {
                showError('Ссылка недействительна!')
            }
        })()
    }, [])

    return (
        <div className={styles.addVideMain}>
            <h1 className={styles.title}>Изменить пароль</h1>
            {isAvailableUpdate && <div className={styles.formContainer}>
                <div className="flex flex-column gap-2">
                    <span className="p-input-icon-left p-float-label">
                        <i className="pi pi-key"/>
                        <InputText
                            value={pass}
                            onChange={(e) => setPass(e.target.value)}
                            type="password"
                            id="password"
                            placeholder="Новый пароль"
                        />
                        <label htmlFor="password">Новый пароль</label>
                    </span>
                </div>
                <div className="flex flex-column">
                    <span className="p-input-icon-left p-float-label">
                        <i className="pi pi-key"/>
                        <InputText
                            value={repeatPass}
                            onChange={(e) => setRepeatPass(e.target.value)}
                            id="repeat-password"
                            type="password"
                            placeholder="Повторите пароль"
                        />
                        <label htmlFor="repeat-password">Повторите пароль</label>
                    </span>
                </div>
            </div>}
            {isAvailableUpdate && <Button
                className={styles.loginBtn}
                disabled={!(pass.length > 9 && pass === repeatPass)}
                onClick={updatePassword}
                label="Восстановить пароль"
                icon="pi pi-user"
            />}
            {!isAvailableUpdate && <div>Ссылка недействительна</div>}
        </div>
    )
}
