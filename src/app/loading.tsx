'use client'
import styles from "@/components/Provider/styles.module.css";
import {ProgressBar} from "primereact/progressbar";
import React from "react";

export default function Loading () {
    return <ProgressBar className={styles.progress} mode="indeterminate"/>
}