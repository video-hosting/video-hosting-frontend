'use client'
import {useContext, useState} from "react";
import {AuthContext} from "@/components/Provider";
import {TabMenu} from "primereact/tabmenu";
import {SettingsMenuItem} from "@/components/Settings/utils";
import {Settings} from "@/components/Settings";
import styles from './page.module.css'
import {classNames} from "primereact/utils";

export default function AddVideoPage() {
    const [activeItem, setActiveItem] = useState<number>(0)
    const {user} = useContext(AuthContext);

    if (!user) return null
    return (
        <div>
            <h1 className={styles.title}>Настройки</h1>
            <div className={classNames("card", styles.content)}>
                <TabMenu activeIndex={activeItem} onTabChange={e => setActiveItem(e.index)} model={SettingsMenuItem} />
                <div className={styles.itemContent}>
                    <Settings activeIndex={activeItem}/>
                </div>
            </div>
        </div>
    )
}
