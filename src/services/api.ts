import axios, {AxiosRequestConfig, AxiosResponse} from 'axios'
import {IUserForVideo, IUserFromLogin} from "@/types/UserTypes";
import {authController} from "@/utils/AuthController";
import {ILikesChanged, IVideoWithUserLikes} from "@/types/VideoTypes";
import {AddCommentRequestT, IChildrenComment, IComment} from "@/types/CommentsTypes";
import {InputsType} from "@/components/AddVideoContent/components/AddVideoData";

const isProd = process.env.NODE_ENV === 'production'
export const BASE_URL =  isProd ? '' : 'http://localhost:4400'
export const BASE_URL_FOR_NEXT_SERVER = 'http://localhost:4400'

export const api = {
    signup: (data: any): Promise<AxiosResponse<IUserFromLogin & {accessToken: string}>['data']> => (
        axios.post(`${BASE_URL}/api/signup`, data, {
            withCredentials: true
        }).then(r =>r.data)
    ),
    signin: (data?: any): Promise<AxiosResponse<IUserFromLogin & {accessToken: string}>['data']> => axios.post(`${BASE_URL}/api/sign-in`, data, {withCredentials: true}).then(r =>r.data),
    logout: () => axios.get(`${BASE_URL}/api/logout`, {withCredentials: true}),
    confirmAccount: (key: string) => axios.get(`${BASE_URL}/api/confirm-account`, {
        params: {key},
        withCredentials: true,
    }),
    updateAccessToken: () => axios.get(`${BASE_URL}/api/update-access-token`, {
        withCredentials: true,
    }),
    resetPassword: (login: string) => axios.get(`${BASE_URL}/api/reset-password`, {
        params: {login}
    }),
    checkLinkResetPassword: (key:string) => axios.get(`${BASE_URL}/api/check-link-reset-password`, {
        params: {key}
    }),
    updatePassword: (passwordHash: string, key: string) => axios.put(
        `${BASE_URL}/api/update-password`,
        {passwordHash, key}
    ),
    updateAvatar: (formData: FormData) => axios.put(`${BASE_URL}/api/user/update-avatar`, formData, {
        headers: {Authorization: authController.getAccessToken()}
    }),
    uploadVideo: (
        data: FormData,
        onUploadProgress: AxiosRequestConfig['onUploadProgress']
    ) => axios.post(`${BASE_URL}/api/video`, data, {
        headers: {Authorization: authController.getAccessToken()},
        onUploadProgress
    }),
    updateVideo: (id: number, data: Omit<InputsType, 'tags'> & {tags:string} ) => axios.put(`${BASE_URL}/api/video/${id}`, data, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    deleteVideo: (id: number) => axios.delete(`${BASE_URL}/api/video/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    getVideos: (q? :string) => axios.get(`${BASE_URL}/api/video/list`, {
        headers: {Authorization: authController.getAccessToken()},
        params: {q}
    }),
    getModerateVideos: () => axios.get(`${BASE_URL}/api/video/moderate-list`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    setAgeControl: (id: number, value: boolean) => axios.put<boolean>(`${BASE_URL}/api/video/set-age-control/${id}`,
        {isAgeRelatedContent: value},
        {headers: {Authorization: authController.getAccessToken()}}
    ),
    approveVideo: (id: number) => axios.put<boolean>(`${BASE_URL}/api/video/approve/${id}`,
        {isModerating: false},
        {headers: {Authorization: authController.getAccessToken()}}
    ),
    getVideoDataServer: (id: string) => axios.get<IVideoWithUserLikes>(`${BASE_URL_FOR_NEXT_SERVER}/api/video/${id}`),
    getChannelInfoServer: (id: string) => axios.get<
        IUserForVideo & {subscribersCount: number}
    >(`${BASE_URL_FOR_NEXT_SERVER}/api/channel/${id}`),
    setLike: (id: string) => axios.get<ILikesChanged>(`${BASE_URL}/api/likes/like/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    deleteLike: (id: string) => axios.delete<ILikesChanged>(`${BASE_URL}/api/likes/like/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    setDislike: (id: string) => axios.get<ILikesChanged>(`${BASE_URL}/api/likes/dislike/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    deleteDislike: (id: string) => axios.delete<ILikesChanged>(`${BASE_URL}/api/likes/dislike/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    addComment: (data: AddCommentRequestT, videoId: string) => axios
        .post<IComment | IChildrenComment>(`${BASE_URL}/api/comments/${videoId}`, data, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    getChildrenComments: (parentCommentId: number) => axios.get(`${BASE_URL}/api/comments/children-comments/${parentCommentId}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    setLikeComment: (id: number) => axios.get<ILikesChanged>(`${BASE_URL}/api/likes/like-comment/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    deleteLikeComment: (id: number) => axios.delete<ILikesChanged>(`${BASE_URL}/api/likes/like-comment/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    setDislikeComment: (id: number) => axios.get<ILikesChanged>(`${BASE_URL}/api/likes/dislike-comment/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    deleteDislikeComment: (id: number) => axios.delete<ILikesChanged>(`${BASE_URL}/api/likes/dislike-comment/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    subscribe: (id: number) => axios.get(`${BASE_URL}/api/subscribes/subscribe/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    unsubscribe: (id: number) => axios.delete(`${BASE_URL}/api/subscribes/subscribe/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    setView: (id: string) => axios.get(`${BASE_URL}/api/views/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
    getRelatedVideo: (videoId: string) => axios.get(`${BASE_URL}/api/video/related/${videoId}`),
    getVideoData: (id: string) => axios.get<IVideoWithUserLikes>(`${BASE_URL}/api/video/${id}`, {
        headers: {Authorization: authController.getAccessToken()},
    }),
}