import {IUserForVideo} from "@/types/UserTypes";
import {IComment} from "@/types/CommentsTypes";

export interface IVideoItem {
    id: number;
    duration: number;
    likes: number;
    dislikes: number;
    views: number;
    viewsCount: number;
    name: string;
    createdAt: string;
    description: string;
    tags: string[];
    isPrivate?: boolean;
    isModerating?: boolean;
    isAgeRelatedContent?: boolean;
    user: IUserForVideo;
    comments: IComment[];
}

export interface IVideoWithUserLikes extends IVideoItem {
    isLikedVideo?: boolean;
    isDislikedVideo?: boolean;
    user: IVideoItem['user'] & { subscribersCount: number }
}

export interface ILikesChanged {
    likes: number;
    dislikes: number;
    isLiked: boolean;
    isDisliked: boolean;
}