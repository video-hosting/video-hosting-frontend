export enum ThemeE {
    light = 'lara-light-blue',
    dark = 'lara-dark-indigo'
}