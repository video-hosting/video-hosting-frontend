import {IVideoItem} from "@/types/VideoTypes";

export interface IUserFromLogin {
    id: number;
    login: string;
    email: string;
    name: string;
    nickname: string;
    surname: string;
    subscribes: IUserForVideo[];
    accountIsConfirm: boolean;
    role: string[]
}

export interface IUserForVideo {
    id: number,
    nickname: string,
    videos?: IVideoItem[]
}