import {IUserForVideo} from "@/types/UserTypes";

export type AddCommentRequestT = {
    text: string;
    answerUserId?: number;
    parentCommentId?: number;
    replyId?: number;
}

export interface IComment {
    id: number;
    createdAt: string;
    text: string;
    user: IUserForVideo;
    childrenCommentsCount: number;
    dislikesCount: number;
    likesCount: number;
    isDisliked: boolean;
    isLiked: boolean
}
export interface IChildrenComment {
    id: number;
    createdAt: string;
    text: string;
    replyId: number | undefined;
    user: IUserForVideo;
    answerUser: IUserForVideo;
}