module.exports = {
    apps: [
        {
            name: "video-front",
            script: "yarn start",
            args: "limit",
            instances: 1
        }
    ]
}